"""
Convoluting Multilayer Perceptron
"""
__authors__ = "Ian Goodfellow"
__copyright__ = "Copyright 2012-2013, Universite de Montreal"
__credits__ = ["Ian Goodfellow", "David Warde-Farley"]
__license__ = "3-clause BSD"
__maintainer__ = "Ian Goodfellow"

import math
import sys
import warnings
import numpy

import theano

import numpy as np
from theano import config
from theano.compat.python2x import OrderedDict
from theano.gof.op import get_debug_values
from theano.printing import Print
from theano.sandbox.rng_mrg import MRG_RandomStreams
import theano.tensor as T

from pylearn2.models.mlp import *

from pylearn2.costs.mlp import Default
from pylearn2.expr.probabilistic_max_pooling import max_pool_channels
from pylearn2.linear import conv2d
from pylearn2.linear.matrixmul import MatrixMul
from pylearn2.models.model import Model
from pylearn2.expr.nnet import pseudoinverse_softmax_numpy
from pylearn2.space import CompositeSpace
from pylearn2.space import Conv2DSpace
from pylearn2.space import Space
from pylearn2.space import VectorSpace
from pylearn2.utils import function
from pylearn2.utils import py_integer_types
from pylearn2.utils import safe_union
from pylearn2.utils import safe_zip
from pylearn2.utils import sharedX
from pylearn2.utils import wraps

warnings.warn("MLP changing the recursion limit.")
# We need this to be high enough that the big theano graphs we make
# when doing max pooling via subtensors don't cause python to complain.
# python intentionally declares stack overflow well before the stack
# segment is actually exceeded. But we can't make this value too big
# either, or we'll get seg faults when the python interpreter really
# does go over the stack segment.
# IG encountered seg faults on eos3 (a machine at LISA labo) when using
# 50000 so for now it is set to 40000.
# I think the actual safe recursion limit can't be predicted in advance
# because you don't know how big of a stack frame each function will
# make, so there is not really a "correct" way to do this. Really the
# python interpreter should provide an option to raise the error
# precisely when you're going to exceed the stack segment.
sys.setrecursionlimit(40000)

class MLPConv(MLP):
    """
    A multilayer perceptron.

    Note that it's possible for an entire MLP to be a single layer of a larger
    MLP.
    """

    def __init__(self, input_space, layers = [], batch_size=1):
        """
        Parameters
        ----------
        layers : list
            A list of Layer objects. The final layer specifies the output space
            of this MLP.
        batch_size : int, optional
            If not specified then must be a positive integer. Mostly useful if
            one of your layers involves a Theano op like convolution that
            requires a hard-coded batch size.
        nvis : int, optional
            Number of "visible units" (input units). Equivalent to specifying
            `input_space=VectorSpace(dim=nvis)`.
        input_space : Space object, optional
            A Space specifying the kind of input the MLP accepts. If None,
            input space is specified by nvis.
        """
        self.input_space = input_space
        self.layers = layers
        self.batch_size = batch_size
        self.force_batch_size = batch_size
        self.freeze_set = set()

    @wraps(Layer.get_default_cost)
    def get_default_cost(self):
        return Default()

#    @wraps(Layer.set_input_space)
#    def set_input_space(self, input_space):
#        self.input_space = input_space
#
    @wraps(Layer.get_output_space)
    def get_output_space(self):
        return Conv2DSpace(shape=[26, 26], num_channels=1, axes=('c', 0, 1, 'b'))

    @wraps(Layer.get_input_space)
    def get_input_space(self):
        return self.input_space

    @wraps(Layer.get_monitoring_channels)
    def get_monitoring_channels(self, data):
        rval = OrderedDict()

        return rval

    @wraps(Layer.get_monitoring_data_specs)
    def get_monitoring_data_specs(self):
        """
        Notes
        -----
        In this case, we want the inputs and targets.
        """
        space = self.get_input_space()
        source = self.get_input_source()
        print 'source is:', source
        return (space, source)

#    @wraps(Layer.get_params)
#    def get_params(self):
#        raise NotImplementedError()
#
#    @wraps(Layer.censor_updates)
#    def censor_updates(self, updates):
#        pass
#
#    @wraps(Layer.get_weights)
#    def get_weights(self):
#        raise NotImplementedError()
#
#    @wraps(Layer.fprop)
#    def fprop(self, state_below, return_all=False):
#        raise NotImplementedError()

    @wraps(Layer.cost)
    def cost(self, X, X_hat):
        fixed_up = self.layers[-1].latest_state.dimshuffle(0, 3, 2, 1)
        adjusted = self.layers[-2].latest_state[:,:fixed_up.shape[1],:fixed_up.shape[2],:]
        diff = fixed_up - adjusted
        sqr_diff = (diff**2).sum()
        sqrt = fixed_up.sum()
        return abs(diff.sum())
        return fixed_up.sum()
        return T.sqrt(sqr_diff)

    def cost_from_X(self, data):
        """
        Computes self.cost, but takes data=(X, Y) rather than Y_hat as an
        argument.

        This is just a wrapper around self.cost that computes Y_hat by
        calling Y_hat = self.fprop(X)

        Parameters
        ----------
        data : WRITEME
        """
        X, _ = data
        latest = X

        first_layer = self.layers[0]

        for j, l in enumerate(self.layers):
            latest = l.fprop(latest)

            if l == first_layer:
                latest = latest.dimshuffle(*[self.input_space.axes.index(i) for i in l.get_input_space().axes])
            else:
                latest = latest.dimshuffle(*[l.get_output_space().axes.index(i) for i in self.layers[j - 1].get_input_space().axes])

        cost = self.cost(X, latest)

        return cost
