from collections import OrderedDict

import numpy

from pylearn2.models.mlp import MLP
from pylearn2.models.mlp import ConvElemwise
from pylearn2.models.mlp import RectifierConvNonlinearity
from pylearn2.models.mlp import SpaceConverter
from pylearn2.models.mlp import Linear, Sigmoid, RectifiedLinear, Softmax
from pylearn2.models.mlp import CompositeLayer

from pylearn2.costs.cost import Cost

from pylearn2.space import Conv2DSpace, VectorSpace, CompositeSpace

from pylearn2.models.model import Model

from pylearn2.utils import sharedX, wraps

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

trng = RandomStreams(seed=9999)

pi = sharedX(numpy.pi)

#dset = HDF5Dataset(filename='/Tmp/zumerjer/train_small.h5', X_topo='/Data/X', y='/Data/y')

class m_cost(Cost):
    def __init__(self):
        pass

    def expr(self, model, data, **kwargs):
        x, y = data
        X = x.reshape((x.shape[0], model.n_vis))

        label_probs = model.part2.fprop(X)
        label_probs /= label_probs.sum(axis=1).reshape((label_probs.shape[0], 1))
        label = label_probs
        #label = trng.multinomial(n=1, pvals=label_probs).reshape((label_probs.shape[0], 10))

        #label = disconnected_grad(label)
        label -= label.max(axis=1).reshape((label.shape[0], 1))
        label = label/(label.max(axis=1) + 0.00001).reshape((label.shape[0], 1))
        label = T.maximum(label, 0)
        #these 3 steps perform 1-WTA. and renormalize.
        #If there are multiple winners, everybody loses.
        mu, log_sigma = model.phi(X)

        ipt = model.qz((mu, log_sigma), n_samples=model.n_feats)

        X = X.dimshuffle('x', 0, 1)
        S = model.theta(T.concatenate([ipt.mean(axis=0), label], axis=1))
        log_pxz = -(X*T.nnet.softplus(-S) + (1-X)*T.nnet.softplus(S)).sum(axis=2)
        log_pz = -0.5 * (
                T.log(2 * pi) + ipt ** 2
            ).sum(axis=2)
        log_qzx = -0.5*(T.log(2*pi) + 2*log_sigma + (ipt - mu) ** 2 /
                T.exp(2*log_sigma)).sum(axis=2)
        #log_q_py = T.log(label_probs[T.arange(label.shape[0]), label.argmax(axis=1)])

        log_q_pt = T.log(label_probs[T.arange(label_probs.shape[0]), y.argmax(axis=1)])

        the_cost = (log_qzx - log_pz).mean(axis=0), log_pxz.mean(axis=0)
        alpha = 1.0
        lbda = 1.0
        gamma = 1.0

        if 'return_parts' in kwargs and kwargs['return_parts']:
            return -lbda*the_cost[0].mean(), gamma*the_cost[1].mean(), alpha*log_q_pt.mean()
        else:
            return -(gamma*the_cost[1] - lbda*the_cost[0] + alpha*log_q_pt + T.cast(T.log([1.0/model.n_classes]*x.shape[0]), theano.config.floatX)).mean()

    def __call__(self, model, data, **kwargs):
        return self.expr(model, data, **kwargs)

    def get_data_specs(self, model):
        return (CompositeSpace(
            [Conv2DSpace(shape=[48,48], channels=3),
            VectorSpace(dim=2)]),
            ('features', 'targets'))

class m(Model):
    def __init__(self):
        super(m, self).__init__()

        self.n_feats = 1
        self.n_hid = 512
        self.n_vis = 6912
        self.n_classes = 2

        self.encoder = MLP(target_source='features',
                #input_space=Conv2DSpace(shape=[96,96], num_channels=3),
                input_space=VectorSpace(dim=self.n_vis),
                layers=[
                SpaceConverter(
                    layer_name='flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[48,48],
                        channels=3
                )),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='h1',
                    irange=0.01,
                    tied_b=0,
                    output_channels=16,
                    border_mode='valid',
                    kernel_shape=[5,5],
                    kernel_stride=[1,1],
                    pool_shape=[3,3],
                    pool_stride=[2,2],
                    pool_type='max'
                ),
                #    ConvElemwise(
                #        nonlinearity=RectifierConvNonlinearity(),
                #        layer_name='h12',
                #        irange=0.01,
                #        tied_b=0,
                #        output_channels=32,
                #        border_mode='valid',
                #        kernel_shape=[5,5],
                #        kernel_stride=[1,1],
                #        pool_shape=[3,3],
                #        pool_stride=[2,2],
                #        pool_type='max'
                #    ),
                #    ConvElemwise(
                #        nonlinearity=RectifierConvNonlinearity(),
                #        layer_name='h13',
                #        irange=0.01,
                #        tied_b=0,
                #        output_channels=64,
                #        border_mode='valid',
                #        kernel_shape=[5,5],
                #        kernel_stride=[1,1],
                #        pool_shape=[3,3],
                #        pool_stride=[2,2],
                #        pool_type='max'
                #    ),
                CompositeLayer(
                    layer_name='phi',
                    layers=[
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='h2',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=128,
                                    border_mode='valid',
                                    kernel_shape=[5,5],
                                    kernel_stride=[1,1],
                                    pool_shape=[3,3],
                                    pool_stride=[2,2],
                                    pool_type='max'
                                ),
                                Linear(
                                    dim=self.n_hid,
                                    layer_name='lin2',
                                    irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='h3',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=128,
                                    border_mode='valid',
                                    kernel_shape=[5,5],
                                    kernel_stride=[1,1],
                                    pool_shape=[3,3],
                                    pool_stride=[2,2],
                                    pool_type='max'
                                ),
                                Linear(
                                    dim=self.n_hid,
                                    layer_name='lin3',
                                    irange=0.01
                                )
                            ]
                        )
                    ]
                )
            ])

        self.decoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_hid+self.n_classes),
                layers=[
                Linear(
                    layer_name='d_adapter',
                    irange=0.01,
                    dim=self.n_hid
                ),
                SpaceConverter(
                    layer_name='d_flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[2,2],
                        channels=128
                    )
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='d_h0',
                    irange=0.01,
                    tied_b=0,
                    output_channels=64,
                    border_mode='full',
                    kernel_shape=[13,13],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='d_h01',
                    irange=0.01,
                    tied_b=0,
                    output_channels=64,
                    border_mode='full',
                    kernel_shape=[13,13],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='d_h02',
                    irange=0.01,
                    tied_b=0,
                    output_channels=32,
                    border_mode='full',
                    kernel_shape=[13,13],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='d_h03',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=32,
                #    border_mode='full',
                #    kernel_shape=[13,13],
                #    kernel_stride=[1,1],
                #    pool_shape=[1,1],
                #    pool_stride=[1,1],
                #    pool_type='max'
                #),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='d_h04',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=16,
                #    border_mode='full',
                #    kernel_shape=[13,13],
                #    kernel_stride=[1,1],
                #    pool_shape=[1,1],
                #    pool_stride=[1,1],
                #    pool_type='max'
                #),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='d_h05',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=16,
                #    border_mode='full',
                #    kernel_shape=[13,13],
                #    kernel_stride=[1,1],
                #    pool_shape=[1,1],
                #    pool_stride=[1,1],
                #    pool_type='max'
                #),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='d_h06',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=16,
                #    border_mode='full',
                #    kernel_shape=[13,13],
                #    kernel_stride=[1,1],
                #    pool_shape=[1,1],
                #    pool_stride=[1,1],
                #    pool_type='max'
                #),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='d_h07',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=3,
                #    border_mode='full',
                #    kernel_shape=[12,12],
                #    kernel_stride=[1,1],
                #    pool_shape=[1,1],
                #    pool_stride=[1,1],
                #    pool_type='max'
                #),
                Linear(
                    dim=self.n_vis,
                    layer_name='d_lin',
                    irange=0.01
                    ),
                ])


        self.part2 = MLP(target_source='targets',
                #input_space=Conv2DSpace(shape=[96,96], num_channels=3),
                input_space=VectorSpace(dim=self.n_vis),
                layers=[
                SpaceConverter(output_space=Conv2DSpace(shape=(48,48), channels=3), layer_name='space_fix'),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='p_h1',
                    irange=0.01,
                    tied_b=0,
                    output_channels=16,
                    border_mode='valid',
                    kernel_shape=[5,5],
                    kernel_stride=[1,1],
                    pool_shape=[3,3],
                    pool_stride=[2,2],
                    pool_type='max'
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='p_h12',
                    irange=0.01,
                    tied_b=0,
                    output_channels=32,
                    border_mode='valid',
                    kernel_shape=[5,5],
                    kernel_stride=[1,1],
                    pool_shape=[3,3],
                    pool_stride=[2,2],
                    pool_type='max'
                ),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='p_h13',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=64,
                #    border_mode='valid',
                #    kernel_shape=[5,5],
                #    kernel_stride=[1,1],
                #    pool_shape=[3,3],
                #    pool_stride=[2,2],
                #    pool_type='max'
                #),
                #ConvElemwise(
                #    nonlinearity=RectifierConvNonlinearity(),
                #    layer_name='p_h2',
                #    irange=0.01,
                #    tied_b=0,
                #    output_channels=128,
                #    border_mode='valid',
                #    kernel_shape=[5,5],
                #    kernel_stride=[1,1],
                #    pool_shape=[3,3],
                #    pool_stride=[2,2],
                #    pool_type='max'
                #),
                #Sigmoid(
                #    dim=self.n_hid,
                #    layer_name='p_s0',
                #    irange=0.01
                #),
                #Sigmoid(
                #    dim=self.n_hid,
                #    layer_name='p_s1',
                #    irange=0.01
                #),
                Softmax(
                    n_classes=self.n_classes,
                    layer_name='p2_out',
                    irange=0.01
                )
            ])

        self.input_space = self.encoder.input_space
        self.output_space = self.part2.get_output_space()
        self.dropout_fprop = self.encoder.dropout_fprop
        self.cost_obj = m_cost()
        self.cost = lambda x, y: self.cost_obj(self, (x, y))

        self._params = self.encoder.get_params() + self.decoder.get_params() + self.part2.get_params()

    def phi(self, x):
        return self.encoder.fprop(x)

    def theta(self, x):
        return self.decoder.fprop(x)

    def qz(self, p, n_samples=1): #q(z | x)
        mu, log_sigma = p

        epsilon = trng.normal(size=[n_samples, mu.shape[0], self.n_hid], dtype=theano.config.floatX)

        mu = mu.dimshuffle('x', 0, 1)
        log_sigma = log_sigma.dimshuffle('x', 0, 1)

        return mu + T.exp(log_sigma) * epsilon
        
    def px(self, t, n_samples=1): #p(x | z)
        return T.nnet.sigmoid(t[0].dimshuffle('x', 0, 1)) > trng.uniform(
                    size=(n_samples, t[0].shape[0], self.n_vis),
                    dtype=theano.config.floatX
                )

    def sample(self, label=0, n=1, z=None):
        y_hat = numpy.zeros((n, self.n_classes))
        y_hat[:,label] = 1
        if z is None:
            ipt = trng.normal(size=(1, n, self.n_hid))
        else:
            ipt = z

        S = self.theta(T.concatenate([ipt.mean(axis=0), y_hat], axis=1))

        return self.px((S,), n_samples=100).mean(axis=0)

    def continue_learning(self):
        return True

    def get_default_cost(self):
        return m_cost()

    def train_batch(self, dataset, batch_size):
        return True

    def get_weights(self):
        return self.encoder.get_weights()

    def get_monitoring_data_specs(self):
        return (CompositeSpace(
            [Conv2DSpace(shape=[48,48], channels=3),
            VectorSpace(dim=2)]),
            ('features', 'targets'))

    @wraps(Model.get_monitoring_channels)
    def get_monitoring_channels(self, data):
        X, y = data
        rval = OrderedDict()

        parts = self.get_default_cost()(self, data, return_parts=True)
        rval['kl_term'] = parts[0]
        rval['e_term'] = parts[1]
        rval['y_term'] = parts[2]

        mu, log_sigma = self.phi(T.concatenate([X.reshape((X.shape[0], self.n_vis)), y], axis=1))
        ipt = self.qz((mu, log_sigma), n_samples=self.n_feats).dimshuffle(1, 0, 2)
        label = self.part2.fprop(X.reshape((X.shape[0], self.n_vis)))

        rval['error'] = T.neq(y.argmax(axis=1), label.argmax(axis=1)).mean(dtype=theano.config.floatX)
        #rval['Eq_y_lik'] = label[T.arange(label.shape[0]), y.argmax(axis=1)].mean(dtype=theano.config.floatX)

        return rval

    def _modify_updates(self, updates):
        self.part2.modify_updates(updates)
        self.decoder.modify_updates(updates)
        self.encoder.modify_updates(updates)
