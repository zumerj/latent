from collections import OrderedDict

import numpy

from pylearn2.models.mlp import MLP
from pylearn2.models.mlp import ConvElemwise
from pylearn2.models.mlp import RectifierConvNonlinearity
from pylearn2.models.mlp import SpaceConverter
from pylearn2.models.mlp import Linear, Sigmoid, RectifiedLinear
from pylearn2.models.mlp import CompositeLayer

from pylearn2.costs.cost import Cost

from pylearn2.space import Conv2DSpace, VectorSpace, CompositeSpace

from pylearn2.models.model import Model

from pylearn2.utils import sharedX, wraps

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from theano.gradient import disconnected_grad

trng = RandomStreams(seed=9999)

pi = sharedX(numpy.pi)

#dset = HDF5Dataset(filename='/Tmp/zumerjer/train_small.h5', X_topo='/Data/X', y='/Data/y')

class m_cost(Cost):
    def __init__(self):
        pass

    def expr(self, model, data, **kwargs):
        x, y = data
        X = x.reshape((x.shape[0], model.n_vis))

        label_probs = model.part2.fprop(X)
        label_probs /= label_probs.sum(axis=1).reshape((label_probs.shape[0], 1))
        label = label_probs
        #label = trng.multinomial(n=1, pvals=label_probs)

        #label = disconnected_grad(label)
        label -= label.max(axis=1).reshape((label.shape[0], 1))
        label = label/(label.max(axis=1) + 0.00001).reshape((label.shape[0], 1))
        label = T.maximum(label, 0)
        #these 3 steps perform 1-WTA. and renormalize.
        #If there are multiple winners, everybody loses.
        mu, log_sigma = model.phi(T.concatenate([X, label], axis=1))

        ipt = model.qz((mu, log_sigma), n_samples=model.n_feats)

        X = X.dimshuffle('x', 0, 1)
        S = model.theta(T.concatenate([ipt.mean(axis=0), label], axis=1))
        log_pxz = -(X*T.nnet.softplus(-S) + (1-X)*T.nnet.softplus(S)).sum(axis=2)
        log_pz = -0.5 * (
                T.log(2 * pi * T.exp(2 * model.log_prior_sigma)) + ((ipt - model.prior_mu)/T.exp(model.log_prior_sigma)) ** 2
            ).sum(axis=2)
        log_qzx = -0.5*(T.log(2*pi) + 2*log_sigma + (ipt - mu) ** 2 /
                T.exp(2*log_sigma)).sum(axis=2)
        #log_q_py = T.log(label_probs[T.arange(label.shape[0]), label.argmax(axis=1)])

        log_q_pt = T.log(label_probs[T.arange(label_probs.shape[0]), y.argmax(axis=1)] + 1e-7)

        prior_mu, log_prior_sigma = model.prior_mu.dimshuffle('x', 0), model.log_prior_sigma.dimshuffle('x', 0)

        the_cost = (log_prior_sigma - log_sigma + 0.5*(T.exp(2*log_sigma) + (mu - prior_mu)**2) / T.exp(2*log_prior_sigma) - 0.5).sum(axis=1).mean(axis=0), log_pxz.mean(axis=0)
        alpha = 1.0
        lbda = 1.0
        gamma = 1.0

        if 'return_parts' in kwargs and kwargs['return_parts']:
            return map(lambda x: T.cast(x, theano.config.floatX), [-lbda*the_cost[0].mean(), gamma*the_cost[1].mean(), alpha*log_q_pt.mean()])
        else:
            return T.cast(-(gamma*the_cost[1] - lbda*the_cost[0] + alpha*log_q_pt + T.cast(T.log([1.0/model.n_classes]*x.shape[0]), theano.config.floatX)).mean(), theano.config.floatX)

    def __call__(self, model, data, **kwargs):
        return self.expr(model, data, **kwargs)

    def get_data_specs(self, model):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

class m(Model):
    def __init__(self):
        super(m, self).__init__()

        self.n_feats = 1
        self.n_hid = 50
        self.n_vis = 784
        self.n_classes = 10

        self.prior_mu = sharedX(numpy.zeros(self.n_hid))
        self.log_prior_sigma = sharedX(numpy.zeros(self.n_hid))

        self.encoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_vis+self.n_classes),
                layers=[
                RectifiedLinear(
                    layer_name="e_adapter",
                    dim=600,
                    irange=0.01
                ),
                CompositeLayer(
                    layer_name='phi',
                    layers=[
                        MLP(
                            layers=[
                                RectifiedLinear(
                                    dim=600,
                                    layer_name='lin21',
                                    irange=0.01
                                ),
                                RectifiedLinear(
                                    dim=self.n_hid,
                                    layer_name='lin22',
                                    irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                RectifiedLinear(
                                    dim=600,
                                    layer_name='lin31',
                                    irange=0.01
                                ),
                                RectifiedLinear(
                                    dim=self.n_hid,
                                    layer_name='lin32',
                                    irange=0.01
                                )
                            ]
                        )
                    ]
                )
            ])

        self.decoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_hid+self.n_classes),
                layers=[
                RectifiedLinear(
                    dim=600,
                    layer_name='adapter',
                    irange=0.01
                ),
                RectifiedLinear(
                    dim=600,
                    layer_name='d_lin',
                    irange=0.01
                ),
                RectifiedLinear(
                    dim=self.n_vis,
                    layer_name='d_out',
                    irange=0.01
                )
                ])

        self.part2 = MLP(input_space=VectorSpace(dim=self.n_vis),
            layers=[
                RectifiedLinear(dim=500, layer_name='part2_1', irange=0.01),
                RectifiedLinear(dim=self.n_classes, layer_name='part2_2', irange=0.01)
            ])

        self._params = self.encoder.get_params() + self.decoder.get_params() + self.part2.get_params()

    def phi(self, x):
        return self.encoder.fprop(x)

    def theta(self, x):
        return self.decoder.fprop(x)

    def qz(self, p, n_samples=1, n=0): #q(z | x)
        mu, log_sigma = p
        if n == 0:
            n = mu.shape[0]

        epsilon = trng.normal(size=[n_samples, n, self.n_hid], dtype=theano.config.floatX)

        mu = mu.dimshuffle('x', 0, 1)
        log_sigma = log_sigma.dimshuffle('x', 0, 1)

        return mu + T.exp(log_sigma) * epsilon

    def px(self, t, n_samples=1): #p(x | z)
        return T.nnet.sigmoid(t[0].dimshuffle('x', 0, 1)) > trng.uniform(
                    size=(n_samples, t[0].shape[0], self.n_vis),
                    dtype=theano.config.floatX
                )

    def sample(self, label=0, n=1):
        y_hat = numpy.zeros((n, self.n_classes))
        y_hat[:,label] = 1
        ipt = self.qz((self.prior_mu.dimshuffle('x', 0), self.log_prior_sigma.dimshuffle('x', 0)), n_samples=self.n_feats, n=n)

        S = self.theta(T.concatenate([ipt.mean(axis=0), y_hat], axis=1))

        return self.px((S,), n_samples=n).mean(axis=0)

    def continue_learning(self):
        return True

    def get_default_cost(self):
        return m_cost()

    def train_batch(self, dataset, batch_size):
        return True

    def get_weights(self):
        return self.encoder.get_weights()

    def get_monitoring_data_specs(self):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

    @wraps(Model.get_monitoring_channels)
    def get_monitoring_channels(self, data):
        X, y = data
        rval = OrderedDict()

        parts = self.get_default_cost()(self, data, return_parts=True)
        rval['kl_term'] = parts[0]
        rval['e_term'] = parts[1]
        rval['y_term'] = parts[2]

        label = self.part2.fprop(X.reshape((X.shape[0], self.n_vis)))

        rval['error'] = T.neq(y.argmax(axis=1), label.argmax(axis=1)).mean(dtype=theano.config.floatX)
        #rval['Eq_y_lik'] = label[T.arange(label.shape[0]), y.argmax(axis=1)].mean(dtype=theano.config.floatX)

        return rval

    def _modify_updates(self, updates):
        self.part2.modify_updates(updates)
        self.decoder.modify_updates(updates)
        self.encoder.modify_updates(updates)
