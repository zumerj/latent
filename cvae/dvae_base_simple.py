from collections import OrderedDict

import numpy

from pylearn2.models.mlp import MLP
from pylearn2.models.mlp import ConvElemwise
from pylearn2.models.mlp import RectifierConvNonlinearity
from pylearn2.models.mlp import SpaceConverter
from pylearn2.models.mlp import Linear, Sigmoid, RectifiedLinear
from pylearn2.models.mlp import CompositeLayer

from pylearn2.costs.cost import Cost

from pylearn2.space import Conv2DSpace, VectorSpace, CompositeSpace

from pylearn2.models.model import Model

from pylearn2.utils import sharedX, wraps

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

trng = RandomStreams(seed=9999)

pi = sharedX(numpy.pi)

#dset = HDF5Dataset(filename='/Tmp/zumerjer/train_small.h5', X_topo='/Data/X', y='/Data/y')

class m_cost(Cost):
    def __init__(self):
        pass

    def expr(self, model, data, **kwargs):
        x, y = data
        X = x.reshape((x.shape[0], model.n_vis))

        label_probs = model.part2.fprop(X)
        label_probs /= label_probs.sum(axis=1).reshape((label_probs.shape[0], 1))
        label = label_probs
        #label = trng.multinomial(n=1, pvals=label_probs).reshape((label_probs.shape[0], 10))

        #label = disconnected_grad(label)
        label -= label.max(axis=1).reshape((label.shape[0], 1))
        label = label/(label.max(axis=1) + 0.00001).reshape((label.shape[0], 1))
        label = T.maximum(label, 0)
        #these 3 steps perform 1-WTA. and renormalize.
        #If there are multiple winners, everybody loses.
        mu, log_sigma = model.phi(X)

        ipt = model.qz((mu, log_sigma), n_samples=model.n_feats)

        X = X.dimshuffle('x', 0, 1)
        S = model.theta(T.concatenate([ipt.mean(axis=0), label], axis=1))
        log_pxz = -(X*T.nnet.softplus(-S) + (1-X)*T.nnet.softplus(S)).sum(axis=2)
        log_pz = -0.5 * (
                T.log(2 * pi) + ipt ** 2
            ).sum(axis=2)
        log_qzx = -0.5*(T.log(2*pi) + 2*log_sigma + (ipt - mu) ** 2 /
                T.exp(2*log_sigma)).sum(axis=2)
        #log_q_py = T.log(label_probs[T.arange(label.shape[0]), label.argmax(axis=1)])

        log_q_pt = T.log(label_probs[T.arange(label_probs.shape[0]), y.argmax(axis=1)])

        the_cost = (log_qzx - log_pz).mean(axis=0), log_pxz.mean(axis=0)
        alpha = 0.1
        lbda = 1.0

        if 'return_parts' in kwargs and kwargs['return_parts']:
            return -lbda*the_cost[0].mean(), the_cost[1].mean(), alpha*log_q_pt.mean()
        else:
            return -(the_cost[1] - lbda*the_cost[0] + alpha*log_q_pt + T.cast(T.log([1.0/model.n_classes]*x.shape[0]), theano.config.floatX)).mean()

    def __call__(self, model, data, **kwargs):
        return self.expr(model, data, **kwargs)

    def get_data_specs(self, model):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

class m(Model):
    def __init__(self):
        super(m, self).__init__()

        self.n_feats = 100
        self.n_hid = 768
        self.n_vis = 784
        self.n_classes = 10

        self.encoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_vis),
                layers=[
                SpaceConverter(
                    output_space=Conv2DSpace(shape=[28,28], channels=1),
                    layer_name='converter'
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    border_mode='valid',
                    irange=0.01,
                    tied_b=0,
                    output_channels=3,
                    layer_name='conv',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                Linear(
                    dim=1452,
                    layer_name='lin',
                    irange=0.01
                ),
                CompositeLayer(
                    layer_name='phi',
                    layers=[
                        MLP(
                            layers=[
                                Sigmoid(
                                    dim=self.n_hid,
                                    layer_name='lin2',
                                    irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                Sigmoid(
                                    dim=self.n_hid,
                                    layer_name='lin2',
                                    irange=0.01
                                )
                            ]
                        )
                    ]
                )
            ])

        self.decoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_hid+self.n_classes),
                layers=[
                Linear(
                    dim=self.n_hid,
                    layer_name='adapter',
                    irange=0.01
                ),
                SpaceConverter(
                    output_space=Conv2DSpace(shape=[16,16], channels=3),
                    layer_name='d_converter'
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    border_mode='full',
                    irange=0.01,
                    tied_b=0,
                    output_channels=10,
                    layer_name='d_conv',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                Linear(
                    dim=4840,
                    layer_name='d_lin',
                    irange=0.01
                    ),
                Sigmoid(
                        dim=self.n_vis,
                        layer_name='d_lin2',
                        irange=0.01
                    )
                ])

        self.part2 = MLP(input_space=VectorSpace(dim=self.n_vis),
            layers=[
                RectifiedLinear(dim=int(self.n_vis/5), layer_name='part2_1', irange=0.01),
                RectifiedLinear(dim=int(self.n_vis/10), layer_name='part2_2', irange=0.01),
                Sigmoid(dim=10, layer_name='output', irange=0.01)
            ])

        self._params = self.encoder.get_params() + self.decoder.get_params() + self.part2.get_params()

    def phi(self, x):
        return self.encoder.fprop(x)

    def theta(self, x):
        return self.decoder.fprop(x)

    def qz(self, p, n_samples=1): #q(z | x)
        mu, log_sigma = p

        epsilon = trng.normal(size=[n_samples, mu.shape[0], self.n_hid], dtype=theano.config.floatX)

        mu = mu.dimshuffle('x', 0, 1)
        log_sigma = log_sigma.dimshuffle('x', 0, 1)

        return mu + T.exp(log_sigma) * epsilon
        
    def px(self, t, n_samples=1): #p(x | z)
        return T.nnet.sigmoid(t[0].dimshuffle('x', 0, 1)) > trng.uniform(
                    size=(n_samples, t[0].shape[0], self.n_vis),
                    dtype=theano.config.floatX
                )

    def sample(self, label=0, n=1):
        y_hat = numpy.zeros((n, self.n_classes))
        y_hat[:,label] = 1
        ipt = self.qz((T.zeros((n, self.n_hid)), T.zeros((n, self.n_hid))), n_samples=self.n_feats)

        S = self.theta(T.concatenate([ipt.mean(axis=0), y_hat], axis=1))

        return self.px((S,), n_samples=self.n_feats).mean(axis=0)

    def continue_learning(self):
        return True

    def get_default_cost(self):
        return m_cost()

    def train_batch(self, dataset, batch_size):
        return True

    def get_weights(self):
        return self.encoder.get_weights()

    def get_monitoring_data_specs(self):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

    @wraps(Model.get_monitoring_channels)
    def get_monitoring_channels(self, data):
        X, y = data
        rval = OrderedDict()

        parts = self.get_default_cost()(self, data, return_parts=True)
        rval['kl_term'] = parts[0]
        rval['e_term'] = parts[1]
        rval['y_term'] = parts[2]

        mu, log_sigma = self.phi(T.concatenate([X.reshape((X.shape[0], self.n_vis)), y], axis=1))
        ipt = self.qz((mu, log_sigma), n_samples=self.n_feats).dimshuffle(1, 0, 2)
        label = self.part2.fprop(X.reshape((X.shape[0], self.n_vis)))

        rval['error'] = T.neq(y.argmax(axis=1), label.argmax(axis=1)).mean(dtype=theano.config.floatX)
        #rval['Eq_y_lik'] = label[T.arange(label.shape[0]), y.argmax(axis=1)].mean(dtype=theano.config.floatX)

        return rval

    def _modify_updates(self, updates):
        self.part2.modify_updates(updates)
        self.decoder.modify_updates(updates)
        self.encoder.modify_updates(updates)
