from collections import OrderedDict

import numpy

from pylearn2.models.mlp import MLP
from pylearn2.models.mlp import ConvElemwise
from pylearn2.models.mlp import RectifierConvNonlinearity
from pylearn2.models.mlp import SpaceConverter
from pylearn2.models.mlp import Linear, Sigmoid, RectifiedLinear
from pylearn2.models.mlp import CompositeLayer

from theano.gradient import disconnected_grad

from pylearn2.costs.cost import Cost

from pylearn2.space import Conv2DSpace, VectorSpace, CompositeSpace

from pylearn2.models.model import Model

from pylearn2.utils import sharedX, wraps

import theano
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

trng = RandomStreams(seed=9999)

pi = sharedX(numpy.pi)

#dset = HDF5Dataset(filename='/Tmp/zumerjer/train_small.h5', X_topo='/Data/X', y='/Data/y')

class m_cost(Cost):
    def __init__(self):
        pass

    def expr(self, model, data, **kwargs):
        alpha = 0.1
        lbda = 1.0

        x, y = data
        X = x.reshape((x.shape[0], model.n_vis))

        mu, log_sigma = model.encoder.fprop(X)

        z1_samples = model.sample_n((mu, log_sigma), n_samples=model.n_feats)

        label_probs = model.p2_lab.fprop(z1_samples.mean(axis=0))
        label_probs /= label_probs.sum(axis=1).reshape((label_probs.shape[0], 1))
        label = label_probs
        #label = trng.multinomial(n=1, pvals=label_probs).reshape((label_probs.shape[0], 10))

        #label = disconnected_grad(label)
        label -= label.max(axis=1).reshape((label.shape[0], 1))
        label = label/(label.max(axis=1) + 0.00001).reshape((label.shape[0], 1))
        label = T.maximum(label, 0)
        #these 3 steps perform 1-WTA. and renormalize.
        #If there are multiple winners, everybody loses.

        mu_2, log_sigma_2 = model.p2_encoder.fprop(disconnected_grad(mu))
        z2_samples = model.sample_n((mu_2, log_sigma_2), n_samples=model.n_feats)
        mu_hat, log_sigma_hat = model.p2_decoder.fprop(T.concatenate([mu_2, y], axis=1))
        z_hat_samples = model.sample_n((mu_hat, log_sigma_hat), n_samples=model.n_feats)

        X = X.dimshuffle('x', 0, 1)
        S = model.decoder.fprop(mu)
        log_pxz = -(X*T.nnet.softplus(-S) + (1-X)*T.nnet.softplus(S)).sum(axis=2)
        log_pz2yz = -0.5*(T.log(2*pi) + 2*log_sigma_hat + (z1_samples - mu_hat) ** 2 /
                T.exp(2*log_sigma_hat)).sum(axis=2)

        log_pz = -0.5 * (
                T.log(2 * pi) + (z1_samples ** 2)
            ).sum(axis=2)

        mu_prime, log_sigma_prime = model.p2_decoder.fprop(T.concatenate([T.zeros_like(mu_2), y], axis=1))
        #using z2_samples[0] provides a n=1 monte-carlo estimate of the marginal over z2 of the conditional distribution of z1 under the decoder's distribution

        log_pz2y = -0.5*(T.log(2*pi) + 2*log_sigma_prime + (z_hat_samples - mu_prime) ** 2 /
                T.exp(2*log_sigma_prime)).sum(axis=2)

        log_q_pt = T.log(label_probs[T.arange(label_probs.shape[0]), y.argmax(axis=1)])
        log_qz2x = -0.5*(T.log(2*pi) + 2*log_sigma_2 + (z2_samples - mu_2) ** 2 /
                T.exp(2*log_sigma_2)).sum(axis=2)
        log_qzx = -0.5*(T.log(2*pi) + 2*log_sigma + (z1_samples - mu) ** 2 /
                T.exp(2*log_sigma)).sum(axis=2)
        #log_q_py = T.log(label_probs[T.arange(label.shape[0]), label.argmax(axis=1)])

        log_py = T.cast(T.log([1.0/model.n_classes]*x.shape[0]), theano.config.floatX)

        #the_cost = lbda*(log_qzx - log_pz).mean(axis=0), log_pz2y.mean(axis=0), log_pz2yz.mean(axis=0)

        if 'return_parts' in kwargs and kwargs['return_parts']:
            #return -the_cost[0].mean(), the_cost[1].mean(), -the_cost[2].mean(), alpha*log_q_pt.mean()
            return -lbda*(log_qzx - log_pz).mean(), log_pxz.mean(), -alpha*log_q_pt.mean(), 0
        else:
            return -lbda*0*(log_qzx - log_pz).mean() + log_pxz.mean() - alpha*log_q_pt.mean()
            #return -(the_cost[1].mean() - the_cost[0].mean() - the_cost[2].mean()) - alpha*log_q_pt.mean()

    def __call__(self, model, data, **kwargs):
        return self.expr(model, data, **kwargs)

    def get_data_specs(self, model):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

class m(Model):
    def __init__(self):
        super(m, self).__init__()

        self.n_feats = 100
        self.n_hid = 768
        self.n_vis = 784
        self.n_classes = 10
        self.m2_n_hid = 160

        self.encoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_vis),
                layers=[
                SpaceConverter(
                    layer_name='flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[28,28],
                        channels=1
                )),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='h1',
                    irange=0.01,
                    tied_b=0,
                    output_channels=3,
                    border_mode='valid',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                SpaceConverter(
                    layer_name='conv_to_flat',
                    output_space=VectorSpace(
                        dim=1452
                )),
                Linear(
                    dim=1452,
                    layer_name='lin',
                    irange=0.01
                ),
                SpaceConverter(
                    layer_name='flat_to_conv_2',
                    output_space=Conv2DSpace(
                        shape=[22,22],
                        channels=3
                )),
                CompositeLayer(
                    layer_name='phi',
                    layers=[
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='h2',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=3,
                                    border_mode='valid',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='conv_to_flat_2',
                                    output_space=VectorSpace(
                                        dim=self.n_hid
                                    )
                                ),
                                Linear(
                                    dim=self.n_hid,
                                    layer_name='lin2',
                                    irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='h3',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=3,
                                    border_mode='valid',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='conv_to_flat_2',
                                    output_space=VectorSpace(
                                        dim=self.n_hid
                                    )
                                ),
                                Linear(
                                    dim=self.n_hid,
                                    layer_name='lin2',
                                    irange=0.01
                                )
                            ]
                        )
                    ]
                )
            ])

        self.decoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_hid),
                layers=[
                SpaceConverter(
                    layer_name='d_flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[16,16],
                        channels=3
                    )
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='d_h0',
                    irange=0.01,
                    tied_b=0,
                    output_channels=10,
                    border_mode='full',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                SpaceConverter(
                    layer_name='d_conv_to_flat',
                    output_space=VectorSpace(
                        dim=4840
                    )
                ),
                Linear(
                    dim=4840,
                    layer_name='d_lin',
                    irange=0.01
                    ),
                SpaceConverter(
                    layer_name='d_flat_to_conv_2',
                    output_space=Conv2DSpace(
                        shape=[22,22],
                        channels=10
                        )),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='d_h1',
                    irange=0.01,
                    tied_b=0,
                    output_channels=1,
                    border_mode='full',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                SpaceConverter(
                    layer_name='d_conv_to_flat_2',
                    output_space=VectorSpace(
                        dim=self.n_vis
                    )
                ),
                Linear(
                        dim=self.n_vis,
                        layer_name='d_lin2',
                        irange=0.01
                    )
                ])


        self.p2_encoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.n_hid),
                layers=[
                SpaceConverter(
                    layer_name='2e_flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[16,16],
                        channels=3
                )),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='2e_h1',
                    irange=0.01,
                    tied_b=0,
                    output_channels=3,
                    border_mode='valid',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                SpaceConverter(
                    layer_name='2e_conv_to_flat',
                    output_space=VectorSpace(
                        dim=300
                )),
                Linear(
                    dim=300,
                    layer_name='2e_lin',
                    irange=0.01
                ),
                SpaceConverter(
                    layer_name='2e_flat_to_conv_2',
                    output_space=Conv2DSpace(
                        shape=[10,10],
                        channels=3
                )),
                CompositeLayer(
                    layer_name='2e_phi',
                    layers=[
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='2e_h2',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=10,
                                    border_mode='valid',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='2e_conv_to_flat_2',
                                    output_space=VectorSpace(
                                        dim=self.m2_n_hid
                                    )
                                ),
                                Linear(
                                    dim=self.m2_n_hid,
                                    layer_name='2e_lin2',
                                    irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='2e_h3',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=10,
                                    border_mode='valid',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='2e_conv_to_flat_2',
                                    output_space=VectorSpace(
                                        dim=self.m2_n_hid
                                    )
                                ),
                                Linear(
                                    dim=self.m2_n_hid,
                                    layer_name='2e_lin2',
                                    irange=0.01
                                )
                            ]
                        )
                    ]
                )
            ])

        self.p2_decoder = MLP(target_source='features',
                input_space=VectorSpace(dim=self.m2_n_hid+self.n_classes),
                layers=[
                Linear(
                    layer_name='2d_adapter',
                    dim=self.m2_n_hid,
                    irange=0.01
                ),
                SpaceConverter(
                    layer_name='2d_flat_to_conv',
                    output_space=Conv2DSpace(
                        shape=[4,4],
                        channels=10
                    )
                ),
                ConvElemwise(
                    nonlinearity=RectifierConvNonlinearity(),
                    layer_name='2d_h0',
                    irange=0.01,
                    tied_b=0,
                    output_channels=10,
                    border_mode='full',
                    kernel_shape=[7,7],
                    kernel_stride=[1,1],
                    pool_shape=[1,1],
                    pool_stride=[1,1],
                    pool_type='max'
                ),
                SpaceConverter(
                    layer_name='2d_conv_to_flat',
                    output_space=VectorSpace(
                        dim=1000
                    )
                ),
                Linear(
                    dim=1000,
                    layer_name='2d_lin',
                    irange=0.01
                    ),
                SpaceConverter(
                    layer_name='2d_flat_to_conv_2',
                    output_space=Conv2DSpace(
                        shape=[10,10],
                        channels=10
                        )),
                CompositeLayer(
                    layer_name='2d_composite',
                    layers=[
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='2d_h1',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=3,
                                    border_mode='full',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='2d_conv_to_flat_2',
                                    output_space=VectorSpace(
                                        dim=self.n_hid
                                    )
                                ),
                                Linear(
                                        dim=self.n_hid,
                                        layer_name='d_lin2',
                                        irange=0.01
                                )
                            ]
                        ),
                        MLP(
                            layers=[
                                ConvElemwise(
                                    nonlinearity=RectifierConvNonlinearity(),
                                    layer_name='2d_h1_s',
                                    irange=0.01,
                                    tied_b=0,
                                    output_channels=3,
                                    border_mode='full',
                                    kernel_shape=[7,7],
                                    kernel_stride=[1,1],
                                    pool_shape=[1,1],
                                    pool_stride=[1,1],
                                    pool_type='max'
                                ),
                                SpaceConverter(
                                    layer_name='2d_conv_to_flat_2_s',
                                    output_space=VectorSpace(
                                        dim=self.n_hid
                                    )
                                ),
                                Linear(
                                        dim=self.n_hid,
                                        layer_name='d_lin2_s',
                                        irange=0.01
                                )
                            ]
                        )
                    ])
                ])

        self.p2_lab = MLP(
                input_space=VectorSpace(dim=self.n_hid),
                layers=[
                    Linear(layer_name='l_lin',
                        dim=self.m2_n_hid,
                        irange=0.01
                    ),
                    Sigmoid(layer_name='label_output',
                        dim=self.n_classes,
                        irange=0.01
                    )
                ])

        self._params = self.encoder.get_params() + self.decoder.get_params() + self.p2_encoder.get_params() + self.p2_decoder.get_params() + self.p2_lab.get_params()

    def sample_n(self, p, n_samples=1): #q(z | x)
        mu, log_sigma = p

        epsilon = trng.normal(size=[n_samples, mu.shape[0], mu.shape[1]], dtype=theano.config.floatX)

        mu = mu.dimshuffle('x', 0, 1)
        log_sigma = log_sigma.dimshuffle('x', 0, 1)

        return mu + T.exp(log_sigma) * epsilon
        
    def sample_bern(self, t, n_samples=1): #p(x | z)
        return T.nnet.sigmoid(t[0].dimshuffle('x', 0, 1)) > trng.uniform(
                    size=(n_samples, t[0].shape[0], t[0].shape[1]),
                    dtype=theano.config.floatX
                )

    def sample(self, label=0, n=1):
        y_hat = numpy.zeros((n, self.n_classes))
        y_hat[:,label] = 1

        z2_sample = self.sample_n((T.zeros((n, self.n_hid)), T.zeros((n, self.n_hid))), n_samples=n)

        mu_hat, sigma_hat = self.p2_decoder.fprop(T.concatenate([z2_sample, y_hat], axis=1), n_samples=self.n_feats)
        z1_sample = self.sample_n((mu_hat, sigma_hat), n_samples=self.n_feats)
        S = self.decoder.fprop(z1_sample.mean(axis=0))

        return self.sample_bern((S,), n_samples=self.n_feats).mean(axis=0)

    def continue_learning(self):
        return True

    def get_default_cost(self):
        return m_cost()

    def train_batch(self, dataset, batch_size):
        return True

    def get_weights(self):
        return self.encoder.get_weights()

    def get_monitoring_data_specs(self):
        return (CompositeSpace(
            [Conv2DSpace(shape=[28,28], channels=1),
            VectorSpace(dim=10)]),
            ('features', 'targets'))

    @wraps(Model.get_monitoring_channels)
    def get_monitoring_channels(self, data):
        X, y = data
        rval = OrderedDict()

        parts = self.get_default_cost()(self, data, return_parts=True)
        rval['kl_term'] = parts[0]
        rval['e_marg_term'] = parts[1]
        rval['e_z2_term'] = parts[2]
        rval['y_term'] = parts[3]

        mu, log_sigma = self.encoder.fprop(X.reshape((X.shape[0], self.n_vis)))
        ipt = self.sample_n((mu, log_sigma), n_samples=self.n_feats).dimshuffle(1, 0, 2)
        label = self.p2_lab.fprop(ipt.mean(axis=1))

        rval['error'] = T.neq(y.argmax(axis=1), label.argmax(axis=1)).mean(dtype=theano.config.floatX)
        #rval['Eq_y_lik'] = label[T.arange(label.shape[0]), y.argmax(axis=1)].mean(dtype=theano.config.floatX)

        return rval

    def _modify_updates(self, updates):
        self.p2_lab.modify_updates(updates)
        self.p2_decoder.modify_updates(updates)
        self.p2_encoder.modify_updates(updates)
        self.decoder.modify_updates(updates)
        self.encoder.modify_updates(updates)
