"""
.. todo::

    WRITEME
"""
__authors__ = 'Vincent Archambault-Bouffard, Ian Goodfellow'
__copyright__ = "Copyright 2013, Universite de Montreal"

from theano import tensor as T

from pylearn2.costs.cost import Cost, DefaultDataSpecsMixin, NullDataSpecsMixin
from pylearn2.utils import safe_izip


class Default(DefaultDataSpecsMixin, Cost):
    """
    The default Cost to use with an MLP.
    It simply calls the MLP's cost_from_X method.
    """

    supervised = True

    def expr(self, model, data, **kwargs):
        """
        .. todo::

            WRITEME
        """
        #Don't validate because we don't care about the
        #targets anymore
        print 'The shapes are:', data[0].shape, data[1].shape
        return model.cost_from_X(data)
