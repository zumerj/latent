"""
Autoencoders, denoising autoencoders, and stacked DAEs.
"""
# Standard library imports
import functools
from itertools import izip
import operator

# Third-party imports
import numpy
import theano
from theano import tensor

# Local imports
from pylearn2.base import Block, StackedBlocks
from pylearn2.models import Model
from pylearn2.utils import sharedX
from pylearn2.utils.theano_graph import is_pure_elemwise
from pylearn2.space import VectorSpace, Conv2DSpace

theano.config.warn.sum_div_dimshuffle_bug = False

if 0:
    print 'WARNING: using SLOW rng'
    RandomStreams = tensor.shared_randomstreams.RandomStreams
else:
    import theano.sandbox.rng_mrg
    RandomStreams = theano.sandbox.rng_mrg.MRG_RandomStreams


class Autoencoder(Block, Model):
    """
    Base class implementing ordinary autoencoders.

    More exotic variants (denoising, contracting autoencoders) can inherit
    much of the necessary functionality and override what they need.
    """
    def __init__(self, input_space, kernel_shape, nhid, act_enc, act_dec, num_channels=1, batch_size=1,
                 tied_weights=False, irange=1e-3, rng=9001):
        """
        Allocate an autoencoder object.

        Parameters
        ----------
        nvis : int
            Number of visible units (input dimensions) in this model. \
            A value of 0 indicates that this block will be left partially \
            initialized until later (e.g., when the dataset is loaded and \
            its dimensionality is known).  Note: There is currently a bug \
            when nvis is set to 0. For now, you should not set nvis to 0.
        nhid : int
            Number of hidden units in this model.
        act_enc : callable or string
            Activation function (elementwise nonlinearity) to use for the \
            encoder. Strings (e.g. 'tanh' or 'sigmoid') will be looked up as \
            functions in `theano.tensor.nnet` and `theano.tensor`. Use `None` \
            for linear units.
        act_dec : callable or string
            Activation function (elementwise nonlinearity) to use for the \
            decoder. Strings (e.g. 'tanh' or 'sigmoid') will be looked up as \
            functions in `theano.tensor.nnet` and `theano.tensor`. Use `None` \
            for linear units.
        tied_weights : bool, optional
            If `False` (default), a separate set of weights will be allocated \
            (and learned) for the encoder and the decoder function. If \
            `True`, the decoder weight matrix will be constrained to be equal \
            to the transpose of the encoder weight matrix.
        irange : float, optional
            Width of the initial range around 0 from which to sample initial \
            values for the weights.
        rng : RandomState object or seed
            NumPy random number generator object (or seed to create one) used \
            to initialize the model parameters.
        """
        super(Autoencoder, self).__init__()
        self.input_space = input_space
        self.num_channels = num_channels
        self.output_space = Conv2DSpace(shape=[input_space.shape[0]+kernel_shape[0]-1, input_space.shape[1]+kernel_shape[1]-1], num_channels=self.num_channels, axes=('b', 'c', 0, 1))
        self.kernel_shape = kernel_shape
        self.nhid = nhid

        self.resize_shape = self.output_space.shape[0] * self.output_space.shape[1] * self.output_space.num_channels

        self.batch_size = batch_size

        # Save a few parameters needed for resizing
        self.irange = irange
        self.tied_weights = tied_weights
        if not hasattr(rng, 'randn'):
            self.rng = numpy.random.RandomState(rng)
        else:
            self.rng = rng

        self._initialize_weights(kernel_shape)
        seed = int(self.rng.randint(2 ** 30))
        self.s_rng = RandomStreams(seed)
        if tied_weights and self.weights is not None:
            self.w_prime = self.weights.dimshuffle(1, 0, 2, 3)
            self.Wdec = self.Wenc.T
        else:
            self._initialize_w_prime(kernel_shape)

        self._initialize_visbias(kernel_shape)
        self._initialize_hidbias()

        def _resolve_callable(conf, conf_attr):
            """
            .. todo::

                WRITEME
            """
            if conf[conf_attr] is None or conf[conf_attr] == "linear":
                return None
            # If it's a callable, use it directly.
            if hasattr(conf[conf_attr], '__call__'):
                return conf[conf_attr]
            elif (conf[conf_attr] in globals()
                  and hasattr(globals()[conf[conf_attr]], '__call__')):
                return globals()[conf[conf_attr]]
            elif hasattr(tensor.nnet, conf[conf_attr]):
                return getattr(tensor.nnet, conf[conf_attr])
            elif hasattr(tensor, conf[conf_attr]):
                return getattr(tensor, conf[conf_attr])
            else:
                raise ValueError("Couldn't interpret %s value: '%s'" %
                                    (conf_attr, conf[conf_attr]))

        self.act_enc = _resolve_callable(locals(), 'act_enc')
        self.act_dec = _resolve_callable(locals(), 'act_dec')
        self._params = [
            self.visbias,
            self.hidbias,
            self.weights,
           # self.Wdec,
            self.Wenc,
            self.benc,
            self.bdec
        ]
        if not self.tied_weights:
            self._params.append(self.w_prime)

    def _initialize_weights(self, kernel_shape, rng=None, irange=None):
        """
        .. todo::

            WRITEME
        """
        if rng is None:
            rng = self.rng
        if irange is None:
            irange = self.irange
        # TODO: use weight scaling factor if provided, Xavier's default else
        self.weights = sharedX(
            (.5 - rng.rand(self.num_channels, self.input_space.num_channels, kernel_shape[0], kernel_shape[1])) * irange,
            name='W',
            borrow=True
        )
        self.Wenc = sharedX(
                (.5 - rng.rand(self.nhid, self.resize_shape)) * irange,
                name="Wenc",
                borrow=True)

    def _initialize_hidbias(self):
        """
        .. todo::

            WRITEME
        """
        self.hidbias = sharedX(
            numpy.zeros(self.num_channels),
            name='hb',
            borrow=True
        )
        self.bdec = sharedX(numpy.zeros(self.resize_shape),
                name='bdec',
                borrow=True)

    def _initialize_visbias(self, kernel_shape):
        """
        .. todo::

            WRITEME
        """
        self.visbias = sharedX(
            numpy.zeros(self.input_space.num_channels),
            name='vb',
            borrow=True
        )
        self.benc = sharedX(numpy.zeros(self.nhid),
                name='benc',
                borrow=True)

    def _initialize_w_prime(self, kernel_shape, rng=None, irange=None):
        """
        .. todo::

            WRITEME
        """
        assert not self.tied_weights, (
            "Can't initialize w_prime in tied weights model; "
            "this method shouldn't have been called"
        )
        if rng is None:
            rng = self.rng
        if irange is None:
            irange = self.irange
        self.w_prime = sharedX(
            (.5 - rng.rand(self.input_space.num_channels, self.num_channels, kernel_shape[0], kernel_shape[1])) * irange,
            name='Wprime',
            borrow=True
        )
        self.Wdec = sharedX(
                (.5 - rng.rand(self.resize_shape, self.nhid)) * irange,
                name="Wdec",
                borrow=True)
        self.Wdec = self.Wenc.T

    def _hidden_activation(self, x):
        """
        Single minibatch activation function.

        Parameters
        ----------
        x : tensor_like
            Theano symbolic representing the input minibatch.

        Returns
        -------
        y : tensor_like
            (Symbolic) hidden unit activations given the input.
        """
        if self.act_enc is None:
            act_enc = lambda x: x
        else:
            act_enc = self.act_enc
        return act_enc(self._hidden_input(x))

    def _hidden_input(self, x):
        """
        Given a single minibatch, computes the input to the
        activation nonlinearity without applying it.

        Parameters
        ----------
        x : tensor_like
            Theano symbolic representing the input minibatch.

        Returns
        -------
        y : tensor_like
            (Symbolic) input flowing into the hidden layer nonlinearity.
        """
        #conv_out = s(conv2d(x, W) + b)
        #out = benc + Wenc . conv_out
        self.h1 = self.act_enc(self.hidbias.dimshuffle('x', 0, 'x', 'x') + tensor.nnet.conv.conv2d(x, self.weights, border_mode='full'))
        ret = self.benc + tensor.dot(self.h1.reshape((self.batch_size, self.resize_shape)), self.Wenc.T)
        return ret

    def upward_pass(self, inputs):
        """
        Wrapper to Autoencoder encode function. Called when autoencoder
        is accessed by mlp.PretrainedLayer

        Parameters
        ----------
        inputs : WRITEME

        Returns
        -------
        WRITEME
        """
        return self.encode(inputs)

    def encode(self, inputs):
        """
        Map inputs through the encoder function.

        Parameters
        ----------
        inputs : tensor_like or list of tensor_likes
            Theano symbolic (or list thereof) representing the input \
            minibatch(es) to be encoded. Assumed to be 2-tensors, with the \
            first dimension indexing training examples and the second \
            indexing data dimensions.

        Returns
        -------
        encoded : tensor_like or list of tensor_like
            Theano symbolic (or list thereof) representing the corresponding \
            minibatch(es) after encoding.
        """
        if isinstance(inputs, tensor.Variable):
            return self._hidden_activation(inputs)
        else:
            return [self.encode(v) for v in inputs]

    def decode(self, hiddens):
        """
        Map inputs through the encoder function.

        Parameters
        ----------
        hiddens : tensor_like or list of tensor_likes
            Theano symbolic (or list thereof) representing the input \
            minibatch(es) to be encoded. Assumed to be 2-tensors, with the \
            first dimension indexing training examples and the second \
            indexing data dimensions.

        Returns
        -------
        decoded : tensor_like or list of tensor_like
            Theano symbolic (or list thereof) representing the corresponding \
            minibatch(es) after decoding.
        """
        if self.act_dec is None:
            act_dec = lambda x: x
        else:
            act_dec = self.act_dec
        if isinstance(hiddens, tensor.Variable):
            #pre_out = s(Wdec . x + bdec)
            #deconv_out = b' + W' . pre_out
            self.h1p = act_dec(self.bdec + tensor.dot(hiddens, self.Wdec.T)).reshape((self.batch_size, self.num_channels, self.output_space.shape[0], self.output_space.shape[1]))
            ret = act_dec(self.visbias.dimshuffle('x', 0, 'x', 'x') + tensor.nnet.conv.conv2d(self.h1p, self.w_prime[:,:,::-1,::-1], border_mode='valid'))

            return ret

        else:
            return [self.decode(v) for v in hiddens]

    def reconstruct(self, inputs):
        """
        Reconstruct (decode) the inputs after mapping through the encoder.

        Parameters
        ----------
        inputs : tensor_like or list of tensor_likes
            Theano symbolic (or list thereof) representing the input \
            minibatch(es) to be encoded and reconstructed. Assumed to be \
            2-tensors, with the first dimension indexing training examples \
            and the second indexing data dimensions.

        Returns
        -------
        reconstructed : tensor_like or list of tensor_like
            Theano symbolic (or list thereof) representing the corresponding \
            reconstructed minibatch(es) after encoding/decoding.
        """
        return self.decode(self.encode(inputs))

    def __call__(self, inputs):
        """
        Forward propagate (symbolic) input through this module, obtaining
        a representation to pass on to layers above.

        This just aliases the `encode()` function for syntactic
        sugar/convenience.
        """
        return self.encode(inputs)

    def get_weights(self, borrow=False):
        """
        .. todo::

            WRITEME
        """

        return self.weights.get_value(borrow = borrow)

    def get_weights_format(self):
        """
        .. todo::

            WRITEME
        """

        return ['v', 'h']

    #function overrides
    get_input_space = Model.get_input_space
    get_output_space = Model.get_output_space
