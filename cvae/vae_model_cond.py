from pylearn2.models.vae.conditional import BernoulliVector, Conditional, DiagonalGaussian
from pylearn2.models.mlp import Linear
from pylearn2.space import Conv2DSpace, VectorSpace
import theano
from theano.compat.python2x import OrderedDict
import numpy
from pylearn2.utils import sharedX

class _Data(object):
    def __init__(self):
        pass

#The parameters of the conditionals get wiped repetitively during training.
#Global{P, Q} are used to store these parameters to restore them when
#necessary.
GlobalP = _Data()
GlobalQ = _Data()

seed = 2014*12*11

class ConvConditionalP(BernoulliVector):
    def __init__(self, batch_size, kernel_shape, num_channels, input_space, deconv=None, **kwargs):
        super(BernoulliVector, self).__init__(**kwargs)
        #Store the GlobalP for serialization
        self.data = GlobalP

        self.rng = numpy.random.RandomState(seed)

        self.batch_size = batch_size
        self.deconv = deconv

        self._init_params(kernel_shape, input_space.num_channels, num_channels)
        self.input_space = input_space
        self.inner_space = Conv2DSpace(shape=[input_space.shape[0] + kernel_shape[0] - 1, input_space.shape[1] + kernel_shape[1] - 1], num_channels=num_channels)

        partial_dict = self.__dict__.copy()
        del partial_dict['data']
        GlobalP.params = partial_dict

    def initialize_parameters(self, input_space, ndim):
        super(ConvConditionalP, self).initialize_parameters(input_space, ndim)
        if self.deconv is None:
            self._params.extend([self.W, self.b])

    def _init_params(self, kernel_shape, chan_in, chan_out):
        if self.deconv is not None:
            return
        self.W = sharedX((0.5 - self.rng.rand(chan_in, chan_out, kernel_shape[0], kernel_shape[1])) * 0.001, name='convW_P', borrow=True)
        self.b = sharedX((0.5 - self.rng.rand(chan_in)) * 0.001, name='convb_P', borrow=True)

    def sample_from_conditional(self, conditional_params, epsilon=None, num_samples=None):
        if hasattr(self.data, 'params'):
            self.__dict__.update(self.data.params)
        if epsilon is not None:
            raise ValueError(str(self.__class__) + " is not able to sample " +
                             "using the reparametrization trick.")
        if num_samples is None:
            raise ValueError("number of requested samples needs to be given.")
        # We express mu in terms of the pre-sigmoid activations. See
        # `log_conditional` for more details.
        conditional_probs = theano.tensor.nnet.sigmoid(conditional_params[0])
        #self.ndim represents post-deconv output shape,
        #while we want a pre-deconv sample.
        pre_sample = self.theano_rng.uniform(
            size=(num_samples, self.inner_space.num_channels, self.inner_space.shape[0], self.inner_space.shape[1]),
            dtype=theano.config.floatX)

        if self.deconv is not None:
            sample = self.deconv(pre_sample).reshape([self.batch_size, self.ndim], ndim=2)

        else:
            sample = theano.tensor.nnet.sigmoid(self.b.dimshuffle('x', 0, 'x', 'x') +
                theano.tensor.nnet.conv.conv2d(pre_sample, self.W, border_mode='valid'))\
                .reshape([self.batch_size, self.ndim], ndim=2)

        return (sample < conditional_probs).astype('float32')

    def monitoring_channels_from_conditional_params(self, conditional_params):
        rval = OrderedDict()
        (S, ) = conditional_params
        rval[self.name + '_S_min'] = S.min()
        rval[self.name + '_S_max'] = S.max()
        rval[self.name + '_S_mean'] = S.mean()
        rval[self.name + '_S_std'] = S.std()

        return rval

class ConvConditionalQ(DiagonalGaussian):
    def __init__(self, batch_size, kernel_shape, num_channels, output_space, conv=None, **kwargs):
        super(DiagonalGaussian, self).__init__(**kwargs)
        #Store the GlobalQ for serialization
        self.data = GlobalQ

        self.rng = numpy.random.RandomState(seed)

        self.batch_size = batch_size
        self.conv = conv
        self._init_params(kernel_shape, output_space.num_channels, num_channels)
        self.output_space = output_space
        self.inner_space = Conv2DSpace(shape=[output_space.shape[0] + kernel_shape[0] - 1, output_space.shape[1] + kernel_shape[1] - 1], num_channels=num_channels)

        partial_dict = self.__dict__.copy()
        del partial_dict['data']
        GlobalQ.params = partial_dict

    def initialize_parameters(self, input_space, ndim):
        super(DiagonalGaussian, self).initialize_parameters(input_space, ndim)
        self.mlp.set_input_space(VectorSpace(self.inner_space.shape[0] * self.inner_space.shape[1] * self.inner_space.num_channels))
        if self.conv is None:
            self._params.extend([self.W, self.b])

    def _init_params(self, kernel_shape, chan_in, chan_out):
        if self.conv is not None:
            return
        self.W = sharedX((0.5 - self.rng.rand(chan_out, chan_in, kernel_shape[0], kernel_shape[1]))*0.001, name='convW_Q', borrow=True)
        self.b = sharedX((0.5 - self.rng.rand(chan_out))*0.001, name='convb_Q', borrow=True)

    def encode_conditional_params(self, X):
        if hasattr(self.data, 'params'):
            self.__dict__.update(self.data.params)
        shaped_X = X.reshape((self.batch_size, self.output_space.num_channels, self.output_space.shape[0], self.output_space.shape[1]))
        if self.conv is not None:
            convX = self.conv(shaped_X).reshape([self.batch_size, self.inner_space.num_channels*self.inner_space.shape[0]*self.inner_space.shape[1]])
        else:
            convX = theano.tensor.nnet.sigmoid(self.b.dimshuffle('x', 0, 'x', 'x') +
                theano.tensor.nnet.conv.conv2d(shaped_X, self.W, border_mode='full')).reshape([self.batch_size, self.inner_space.num_channels*self.inner_space.shape[0]*self.inner_space.shape[1]])

        conditional_params = self.mlp.fprop(convX)
        if not type(conditional_params) == tuple:
            conditional_params = (conditional_params, )
        return conditional_params
