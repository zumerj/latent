from dvae_base_2heads_cvd import *

from pylearn2.training_algorithms.learning_rule import AdaDelta
from pylearn2.training_algorithms.sgd import SGD
from pylearn2.datasets.mnist import MNIST

from pylearn2.costs.mlp.dropout import Dropout
from pylearn2.costs.cost import SumOfCosts

from pylearn2.datasets.variable_image_dataset import DogsVsCats
from pylearn2.datasets.variable_image_dataset import RandomCrop
from pylearn2.datasets.hdf5 import HDF5Dataset

from pylearn2.train_extensions.best_params import MonitorBasedSaveBest

from pylearn2.train import Train

#dset = HDF5Dataset(filename='/Tmp/zumerjer/train_small.h5', X_topo='/Data/X', y='/Data/y')

#transf = RandomCrop(scaled_size=98, crop_size=96)
#train_dset = DogsVsCats(transformer=transf, start=0, stop=20000)
#valid_dset = DogsVsCats(transformer=transf, start=20000, stop=22500)
#test_dset = DogsVsCats(transformer=transf, start=22500, stop=25000)

train_dset = HDF5Dataset(filename='/Tmp/zumerjer/very_small_train_10.h5', topo_view='/Data/X', y='/Data/y')
test_dset = HDF5Dataset(filename='/Tmp/zumerjer/very_small_valid_10.h5', topo_view='/Data/X', y='/Data/y')
valid_dset = HDF5Dataset(filename='/Tmp/zumerjer/very_small_test_10.h5', topo_view='/Data/X', y='/Data/y')

model = m()
learning_rule=AdaDelta(decay=0.95)
learning_rate = 1e-7
monitoring_dsets={ 'train': train_dset, 'test': test_dset, 'valid': valid_dset }

t1 = Train(train_dset, model, SGD(batch_size=64, learning_rate=learning_rate, learning_rule=learning_rule, monitoring_dataset=monitoring_dsets, cost=SumOfCosts([model.get_default_cost(), Dropout()])), extensions=[MonitorBasedSaveBest('test_error', "dvae_best.pkl")], save_path="dvae.pkl", save_freq=1)
t1.main_loop()
