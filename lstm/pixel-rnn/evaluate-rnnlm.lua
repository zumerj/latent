require 'nngraph'
require 'rnn'
local dl = require 'dataload'


--[[command line arguments]]--
cmd = torch.CmdLine()
cmd:text()
cmd:text('Evaluate a RNNLM')
cmd:text('Options:')
cmd:option('--xplogpath', '', 'path to a previously saved xplog containing model')
cmd:option('--cuda', false, 'model was saved with cuda')
cmd:option('--device', 1, 'which GPU device to use')
cmd:option('--nsample', -1, 'sample this many words from the language model')
cmd:text()
local opt = cmd:parse(arg or {})

-- check that saved model exists
assert(paths.filep(opt.xplogpath), opt.xplogpath..' does not exist')

if opt.cuda then
   require 'cunn'
   cutorch.setDevice(opt.device)
end

local xplog = torch.load(opt.xplogpath)
local lm = xplog.model
local criterion = xplog.criterion
local targetmodule = xplog.targetmodule
print("Hyper-parameters (xplog.opt):")
print(xplog.opt)

--local trainset, validset, testset = dl.loadPTB({50, 1, 1})
local trainfile = 'ptb.train.txt'
local testfile = 'ptb.test.txt'
local validfile = 'ptb.valid.txt'
local datapath = paths.concat(dl.DATA_PATH, 'PennTreeBank')

local traintext = file.read(paths.concat(datapath, trainfile))
local testtext = file.read(paths.concat(datapath, testfile))
local validtext = file.read(paths.concat(datapath, validfile))

traintext = stringx.replace(traintext, '<unk>', '~')
testtext = stringx.replace(traintext, '<unk>', '~')
validtext = stringx.replace(traintext, '<unk>', '~')

local trainvect = torch.IntTensor(#traintext):fill(0)
local testvect = torch.IntTensor(#testtext):fill(0)
local validvect = torch.IntTensor(#validtext):fill(0)

for i = 1, #traintext do
	trainvect[i] = xplog.conv(traintext:sub(i,i))
end
for i = 1, #testtext do
	testvect[i] = xplog.conv(testtext:sub(i,i))
end
for i = 1, #validtext do
	validvect[i] = xplog.conv(validtext:sub(i,i))
end

local validset = dl.SequenceLoader(validvect, 50)
local trainset = dl.SequenceLoader(trainvect, 50)
local testset = dl.SequenceLoader(testvect, 50)

--assert(trainset.vocab['the'] == xplog.vocab['the'])

print(lm)

lm:forget()
lm:evaluate()

if opt.nsample > 0 then
   local sampletext = {}
--   local prevword = trainset.vocab['<eos>']
   local prevletter = xplog.conv('\n')
--   assert(prevword)
   assert(prevletter)
   local inputs = torch.LongTensor(1,1) -- seqlen x batchsize
   if opt.cuda then inputs = inputs:cuda() end
   local buffer = torch.FloatTensor()
   local burnt = false
   for i=1,opt.nsample do
      --inputs:fill(prevword)
      inputs:fill(prevletter)
      local output = lm:forward(inputs)[1][1]
      buffer:resize(output:size()):copy(output)
      buffer:exp()
      local sample = torch.multinomial(buffer, 1, true)
      --local currentword = trainset.ivocab[sample[1]]
      local currentletter = xplog.unconv(sample[1])
      --table.insert(sampletext, currentword)
      if burnt then
        table.insert(sampletext, currentletter)
      end

      if currentletter == '\n' and not burnt then
        burnt = true
      end
      --prevword = sample[1]
      prevletter = sample[1]
   end
   print(table.concat(sampletext, ''))
else
   local sumErr = 0
   
   for i, inputs, targets in testset:subiter(100) do
      local targets = targetmodule:forward(targets)
      local outputs = lm:forward(inputs)
      local err = criterion:forward(outputs, targets)
      sumErr = sumErr + err
   end

   print(sumErr, sumErr/testset:size())
   local ppl = torch.exp(sumErr/testset:size())
   print("Test PPL : "..ppl)
end

