import numpy
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from collections import OrderedDict

PREFIX = "long_bn"

fh = open("{}.log".format(PREFIX), 'r')

lines = map(lambda x: x.strip().split(), fh.readlines())
dlines = OrderedDict({})

for i, l in enumerate(lines):
    try:
        dlines[int(l[0].split(":")[1])] = [float(l[4].split(":")[1]), float(l[6].split(":")[1])]
    except:
        print 'Error parsing line:', i
        exit(1)

lines = numpy.array(dlines.values()) #only keep one value per epoch
epochs = numpy.array(dlines.keys())

lt, = plt.plot(epochs, lines[:,0], label='Train NLL')
lv, = plt.plot(epochs, lines[:,1], label='Valid NLL')
plt.title("Negative Log-Likelihood Over Epoch")
plt.legend()
plt.savefig("{}_curves.png".format(PREFIX))

fh.close()
