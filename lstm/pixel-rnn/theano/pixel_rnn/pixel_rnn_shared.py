"""
Pixel RNN on MNIST
Ishaan Gulrajani
"""

import os, sys
sys.path.append(os.getcwd())

try: # This only matters on Ishaan's computer
    import experiment_tools
    experiment_tools.wait_for_gpu(high_priority=False)
except ImportError:
    pass

import numpy
numpy.random.seed(123)
import random
random.seed(123)


from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
rng = RandomStreams(123)

import theano
import theano.tensor as T
import lib
import lasagne
import scipy.misc

import time
import functools
import itertools

from collections import OrderedDict

import os
import glob

MODEL = 'pixel_rnn' # either pixel_rnn or pixel_cnn

# Hyperparams
BATCH_SIZE = 16
DIM = 64 # Model dimensionality.("2h"; MNIST: 64 for 1 layer, 32 for 7 layers)
CONV_DIM = 32 # Conv layers output map dimensionality (MNIST: 32)
GRAD_CLIP = 1 # Elementwise grad clip threshold

# Dataset
N_CHANNELS = 1
WIDTH = 28
HEIGHT = 28

# Other constants
TEST_BATCH_SIZE = 100 # batch size to use when evaluating on dev/test sets. This should be the max that can fit into GPU memory.
EVAL_DEV_COST = True # whether to evaluate dev cost during training

GEN_SAMPLES = False # whether to generate samples during training (generating samples takes WIDTH*HEIGHT*N_CHANNELS full passes through the net)
TRAIN_MODE = 'time' # 'iters' to use PRINT_ITERS and STOP_ITERS, 'time' to use PRINT_TIME and STOP_TIME. 'test' outputs performance on the test set instead of training.
RELOAD = True
PREFIX = 'rnn_shared'
if RELOAD:
    RELOAD_PATH = max(glob.iglob('{}_params_*.pkl'.format(PREFIX)), key=os.path.getctime) #use the latest .pkl

PRINT_ITERS = 1000 # Print cost, generate samples, save model checkpoint every N iterations.
STOP_ITERS = 100000 # Stop after this many iterations
PRINT_TIME = 5*60 # Print cost, generate samples, save model checkpoint every N seconds.
STOP_TIME = 60*60*18 # Stop after this many seconds of actual training (not including time req'd to generate samples etc.)

lib.utils.print_model_settings(locals().copy())

def relu(x):
    # Using T.nnet.relu gives me NaNs. No idea why.
    return T.switch(x > lib.floatX(0), x, lib.floatX(0))

def Conv2D(name, input_dim, output_dim, filter_size, inputs, mask_type=None, he_init=False):
    """
    inputs.shape: (batch size, height, width, input_dim)
    mask_type: None, 'a', 'b'
    output.shape: (batch size, height, width, output_dim)
    """
    def uniform(stdev, size):
        """uniform distribution with the given stdev and size"""
        return numpy.random.uniform(
            low=-stdev * numpy.sqrt(3),
            high=stdev * numpy.sqrt(3),
            size=size
        ).astype(theano.config.floatX)

    filters_init = uniform(
        1./numpy.sqrt(input_dim * filter_size * filter_size),
        # output dim, input dim, height, width
        (output_dim, input_dim, filter_size, filter_size)
    )

    if he_init:
        filters_init *= lib.floatX(numpy.sqrt(2.))

    if mask_type is not None:
        filters_init *= lib.floatX(numpy.sqrt(2.))

    filters = lib.param(
        name+'.Filters',
        filters_init
    )

    if mask_type is not None:
        mask = numpy.ones(
            (output_dim, input_dim, filter_size, filter_size), 
            dtype=theano.config.floatX
        )
        center = filter_size//2
        for i in xrange(filter_size):
            for j in xrange(filter_size):
                    if (j > center) or (j==center and i > center):
                        mask[:, :, j, i] = 0.
        for i in xrange(N_CHANNELS):
            for j in xrange(N_CHANNELS):
                if (mask_type=='a' and i >= j) or (mask_type=='b' and i > j):
                    mask[
                        j::N_CHANNELS,
                        i::N_CHANNELS,
                        center,
                        center
                    ] = 0.

        filters = filters * mask

    # conv2d takes inputs as (batch size, input channels, height, width)
    inputs = inputs.dimshuffle(0, 3, 1, 2)
    result = T.nnet.conv2d(inputs, filters, border_mode='half', filter_flip=False)

    biases = lib.param(
        name+'.Biases',
        numpy.zeros(output_dim, dtype=theano.config.floatX)
    )
    result = result + biases[None, :, None, None]

    return result.dimshuffle(0, 2, 3, 1)

def Conv1D(name, input_dim, output_dim, filter_size, inputs, apply_biases=True, typ='valid'):
    """
    inputs.shape: (batch size, height, input_dim)
    output.shape: (batch size, height, output_dim)
    * performs valid convs
    """
    def uniform(stdev, size):
        """uniform distribution with the given stdev and size"""
        return numpy.random.uniform(
            low=-stdev * numpy.sqrt(3),
            high=stdev * numpy.sqrt(3),
            size=size
        ).astype(theano.config.floatX)

    filters = lib.param(
        name+'.Filters',
        uniform(
            1./numpy.sqrt(input_dim * filter_size),
            # output dim, input dim, height, width
            (output_dim, input_dim, filter_size, 1)
        )
    )

    # conv2d takes inputs as (batch size, input channels, height[?], width[?])
    inputs = inputs.reshape((inputs.shape[0], inputs.shape[1], 1, inputs.shape[2]))
    inputs = inputs.dimshuffle(0, 3, 1, 2)
    result = T.nnet.conv2d(inputs, filters, border_mode=typ, filter_flip=False)

    if apply_biases:
        biases = lib.param(
            name+'.Biases',
            numpy.zeros(output_dim, dtype=theano.config.floatX)
        )
        result = result + biases[None, :, None, None]

    result = result.dimshuffle(0, 2, 3, 1)
    return result.reshape((result.shape[0], result.shape[1], result.shape[3]))

def Spiral(inputs):
    """
    input.shape: (batch size, HEIGHT, WIDTH, dim)
    """
    buffer = T.zeros((inputs.shape[0], inputs.shape[1] * inputs.shape[2], inputs.shape[3]))
    center = WIDTH/2 #XXX
    offset = 0
    side = 2

    buffer = T.inc_subtensor(buffer[:, 0, :], inputs[:, center, center, :])
    buffer = T.inc_subtensor(buffer[:, 1, :], inputs[:, center, center - 1, :])
    buffer = T.inc_subtensor(buffer[:, 2, :], inputs[:, center - 1, center - 1, :])
    buffer = T.inc_subtensor(buffer[:, 3, :], inputs[:, center - 1, center, :])

    offset = 4
    xoff = 1
    yoff = -1
    side += 2

    while ((center + yoff + side < HEIGHT) and (center + xoff - 1 < WIDTH)):
        buffer = T.inc_subtensor(buffer[:, offset:offset + side - 1, :], inputs[:, center + yoff + 1:center + yoff + side, center + xoff:center+xoff+1, :].reshape((inputs.shape[0], side-1, inputs.shape[3])))
        buffer = T.inc_subtensor(buffer[:, offset + side - 1:offset + 2*(side - 1), :], inputs[:, center + yoff + side:center+yoff+side+1, center + xoff - side + 2:center + xoff + 1, :][:,:,::-1,:].reshape((inputs.shape[0], side-1, inputs.shape[3])))
        buffer = T.inc_subtensor(buffer[:, offset + 2*(side - 1):offset + 3*(side - 1), :], inputs[:, center + yoff:center + yoff + side - 1, center + xoff - side + 2:center+xoff-side+3, :][:,::-1,:,:].reshape((inputs.shape[0], side-1, inputs.shape[3])))
        buffer = T.inc_subtensor(buffer[:, offset + 3*(side - 1):offset + 4*(side - 1), :], inputs[:, center + yoff:center + yoff+1, center + xoff - side + 1:center + xoff, :].reshape((inputs.shape[0], side-1, inputs.shape[3])))

        offset = offset + 4*(side - 1)
        side += 2
        xoff += 1
        yoff += -1
    return buffer[:,::-1,:].reshape(inputs.shape)

def Skew(inputs):
    """
    input.shape: (batch size, HEIGHT, WIDTH, dim)
    """
    buffer = T.zeros(
        (inputs.shape[0], inputs.shape[1], 2*inputs.shape[2] - 1, inputs.shape[3]),
        theano.config.floatX
    )

    for i in xrange(HEIGHT):
        buffer = T.inc_subtensor(buffer[:, i, i:i+WIDTH, :], inputs[:,i,:,:])

    return buffer

def Unspiral(padded):
    """
    input.shape: (batch size, HEIGHT, 2*WIDTH - 1, dim)
    """
    if isinstance(padded, numpy.ndarray):
        padded = padded.swapaxes(1,2).swapaxes(2,3)
        shape = padded.shape
    else:
        shape = (BATCH_SIZE if TRAIN_MODE != 'test' else TEST_BATCH_SIZE, WIDTH, HEIGHT, N_CHANNELS)
    buffer = T.zeros(padded.shape)
    center = shape[1]/2
    width = shape[1]
    height = shape[2]
    padded = padded.reshape((padded.shape[0], padded.shape[1]*padded.shape[2], padded.shape[3]))[:,::-1,:]
    side = 2

    buffer = T.inc_subtensor(buffer[:, center, center, :], padded[:, 0, :].reshape((padded.shape[0], padded.shape[2])))
    buffer = T.inc_subtensor(buffer[:, center, center - 1, :], padded[:, 1, :].reshape((padded.shape[0], padded.shape[2])))
    buffer = T.inc_subtensor(buffer[:, center - 1, center - 1, :], padded[:, 2, :].reshape((padded.shape[0], padded.shape[2])))
    buffer = T.inc_subtensor(buffer[:, center - 1, center, :], padded[:, 3, :].reshape((padded.shape[0], padded.shape[2])))

    offset = 4
    xoff = 1
    yoff = -1
    side += 2

    while ((center + yoff + side < height) and (center + xoff - 1 < width)):
        buffer = T.inc_subtensor(buffer[:, center + yoff + 1:center + yoff + side, center + xoff:center+xoff+1, :], padded[:, offset:offset + side - 1, :].reshape((padded.shape[0], side - 1, 1, padded.shape[2])))
        buffer = T.inc_subtensor(buffer[:, center + yoff + side:center+yoff+side+1, center + xoff - side + 2:center + xoff + 1, :], padded[:, offset + side - 1:offset + 2*(side - 1), :].reshape((padded.shape[0], 1, side - 1, padded.shape[2]))[:,:,::-1,:])
        buffer = T.inc_subtensor(buffer[:, center + yoff:center + yoff + side - 1, center + xoff - side + 2:center+xoff-side+3, :], padded[:, offset + 2*(side - 1):offset + 3*(side - 1), :].reshape((padded.shape[0], side - 1, 1, padded.shape[2]))[:,::-1,:,:])
        buffer = T.inc_subtensor(buffer[:, center + yoff:center + yoff+1, center + xoff - side + 1:center + xoff, :], padded[:, offset + 3*(side - 1):offset + 4*(side - 1), :].reshape((padded.shape[0], 1, side - 1, padded.shape[2])))

        offset = offset + 4*(side - 1)
        side += 2
        xoff += 1
        yoff += -1

    if isinstance(padded, numpy.ndarray):
        buffer = buffer.dimshuffle(0,3,1,2)

    return buffer

def Unskew(padded):
    """
    input.shape: (batch size, HEIGHT, 2*WIDTH - 1, dim)
    """
    return T.stack([padded[:, i, i:i+WIDTH, :] for i in xrange(HEIGHT)], axis=1)

def DiagonalLSTM(name, input_dim, inputs):
    """
    inputs.shape: (batch size, height, width, input_dim)
    outputs.shape: (batch size, height, width, DIM)
    """
    #recurrent batch-norm from https://github.com/GabrielPereyra/norm-rnn/blob/master/layers.py
    inputs = Skew(inputs)
    batch_size = inputs.shape[0]

    c0_unbatched = lib.param(
        name + '.c0',
        numpy.zeros((HEIGHT, DIM/2), dtype=theano.config.floatX)
    )
    c0 = T.alloc(c0_unbatched, batch_size, HEIGHT, DIM/2)

    h0_unbatched = lib.param(
        name + '.h0',
        numpy.zeros((HEIGHT, DIM), dtype=theano.config.floatX)
    )
    h0 = T.alloc(h0_unbatched, batch_size, HEIGHT, DIM)

    gamma = lib.param(
         name + '.gamma',
         numpy.ones((2*DIM,), dtype='float32')
    )

    beta = lib.param(
         name + '.beta',
         numpy.zeros((2*DIM,), dtype='float32')
    )

    epsilon = 1e-6
    momentum = 0.1

    running_mean = lib.param(name + '.running_mean', numpy.zeros((HEIGHT, WIDTH*2-1, 2*DIM), dtype='float32'))
    running_std = lib.param(name + '.running_std', numpy.zeros((HEIGHT, WIDTH*2-1, 2*DIM), dtype='float32'))
#    running_mean = lib.param(name + '.running_mean', numpy.zeros((HEIGHT, WIDTH, 2*DIM), dtype='float32'))
#    running_std = lib.param(name + '.running_std', numpy.zeros((HEIGHT, WIDTH, 2*DIM), dtype='float32'))

    input_to_state = Conv2D(name+'.InputToState', input_dim, 2*DIM, 1, inputs, mask_type='b')
   
    avg = input_to_state.mean(axis=0)
    std = T.mean((input_to_state - avg) ** 2 + epsilon, axis=0) ** 0.5
    mean_update = momentum * running_mean + (1-momentum) * avg
    std_update = momentum * running_std + (1-momentum) * std
    updates = OrderedDict([(running_mean, mean_update), (running_std, std_update)])

    if TRAIN_MODE != 'test':
        input_to_state = (input_to_state - avg) / (std + epsilon)
    else:
        input_to_state = (input_to_state - mean_update) / (std_update + epsilon)

    input_to_state = input_to_state * gamma + beta
 
    def inner(ipt, c0, h0):
        # all args have shape (batch size, height, DIM)
    
        # TODO consider learning this padding

        prev_h = T.concatenate([
            T.zeros((batch_size, 1, DIM), theano.config.floatX), 
            h0
        ], axis=1)
        state_to_state = Conv1D(name+'.StateToState', DIM, DIM*2, 2, prev_h, apply_biases=False)
    
        gates = state_to_state + ipt
    
        o_f_i = T.nnet.sigmoid(gates[:,:,:3*DIM/2])
        o = o_f_i[:,:,0*DIM/2:1*DIM/2]
        f = o_f_i[:,:,1*DIM/2:2*DIM/2]
        i = o_f_i[:,:,2*DIM/2:3*DIM/2]
        g = T.tanh(gates[:,:,3*DIM/2:4*DIM/2])
    
        new_c = (f * c0) + (i * g)
        pre_new_h = o * T.tanh(new_c)
    
        new_h = Conv1D(name+'.UpSample', DIM/2, DIM, 1, pre_new_h, apply_biases=False)
        new_h = new_h.reshape((batch_size, HEIGHT, DIM))# + ipt
    
        return (new_c, new_h)

    outputs, _ = theano.scan(fn=inner, sequences=[input_to_state.dimshuffle(2,0,1,3)], outputs_info=[c0, h0])
    c = outputs[0]
    h = outputs[1]
    h = h.dimshuffle(1,2,0,3)

    return Unskew(h), updates

def DiagonalBiLSTM(name, input_dim, inputs):
    """
    inputs.shape: (batch size, height, width, input_dim)
    inputs.shape: (batch size, height, width, DIM)
    """
    batch_size = inputs.shape[0]
    forward, fwd_up = DiagonalLSTM(name+'.Forward', input_dim, inputs)
    backward, bwd_up = DiagonalLSTM(name+'.Forward', input_dim, inputs[:,:,::-1,:])
    backward = backward[:,:,::-1,:]
    backward = T.concatenate([
        T.zeros([batch_size, 1, WIDTH, DIM], dtype=theano.config.floatX),
        backward[:, :-1, :, :]
    ], axis=1)

    h = forward + backward

    return h, OrderedDict(fwd_up.items() + bwd_up.items())

# inputs.shape: (batch size, height, width, channels)
inputs = T.tensor4('inputs')
output = Conv2D('InputConv', N_CHANNELS, DIM, 7, inputs, mask_type='a')

extra_updates = {}
if MODEL=='pixel_rnn':
    output, extra_updates = DiagonalBiLSTM('LSTM1', DIM, output)
elif MODEL=='pixel_cnn':
    # The paper doesn't specify how many convs to use, so I picked 10 pretty
    # arbitrarily.
    for i in xrange(10):
        output = Conv2D('PixelCNNConv'+str(i), DIM, DIM, 3, output, mask_type='b', he_init=True)
        output = relu(output)

output = relu(output)
output = Conv2D('OutputConv1', DIM, CONV_DIM, 1, output, mask_type='b', he_init=True)

output = relu(output)
#output = Conv2D('OutputConv2', CONV_DIM, CONV_DIM, 1, output, mask_type='b', he_init=True)

output = Conv2D('OutputConv3', CONV_DIM, N_CHANNELS, 1, output, mask_type='b')
output = T.nnet.sigmoid(output)

#sample = rng.uniform((BATCH_SIZE if TRAIN_MODE != 'test' else TEST_BATCH_SIZE, HEIGHT, WIDTH, N_CHANNELS), low=0., high=1.) < sample

#cost = -T.mean(T.log(T.maximum(1e-6, output))[T.arange(T.prod(inputs.shape)), T.cast(inputs.reshape((T.prod(inputs.shape),)), 'int32')])
#cost = T.mean(T.nnet.categorical_crossentropy(output, theano.tensor.extra_ops.to_one_hot(inputs.swapaxes(0, 1).swapaxes(1, 2).reshape((inputs.shape[0] * HEIGHT * WIDTH, 1)), 256).reshape((inputs.shape[0], HEIGHT, WIDTH, 256)).swapaxes(2, 1).swapaxes(1, 0)))
#cost = T.mean(T.nnet.binary_crossentropy(paramf, inputs), axis=0).sum() + T.mean(T.nnet.binary_crossentropy(paramb, inputs[:,:,::-1,:]), axis=0).sum() - 0.5*(1+2*sigmaf-muf*muf-T.exp(2*sigmaf)).mean() - 0.5*(1+2*sigmab-mub*mub-T.exp(2*sigmab)).mean()
cost = T.mean(T.nnet.binary_crossentropy(T.minimum(1-1e-6, T.maximum(1e-6, output)), inputs), axis=0).sum()

# Reload the parameters instead of training from scratch
if RELOAD:
    lib.load_params(RELOAD_PATH)
    #save a copy of the parameters as a backup to reproduce results later
    lib.save_params("{}_tested_{}".format(PREFIX, RELOAD_PATH))

params = lib.search(cost, lambda x: hasattr(x, 'param'))
lib.utils.print_params_info(params)

if TRAIN_MODE != 'test':
    grads = T.grad(cost, wrt=params, disconnected_inputs='warn')
    grads = [T.clip(g, lib.floatX(-GRAD_CLIP), lib.floatX(GRAD_CLIP)) for g in grads]
    
    updates = lasagne.updates.rmsprop(grads, params, learning_rate=1e-3)
    updates = OrderedDict(updates.items() + extra_updates.items())

    train_fn = theano.function(
        [inputs],
        cost,
        updates=updates,
        on_unused_input='warn'
    )

eval_fn = theano.function(
    [inputs],
    cost,
    on_unused_input='warn'
)

sample_fn = theano.function(
    [inputs],
    output,
    on_unused_input='warn'
)

import fuel, fuel.datasets
#train_data = fuel.datasets.cifar10.CIFAR10(('train',))
#dev_data = fuel.datasets.cifar10.CIFAR10(('test',))
train_data, dev_data, test_data = lib.mnist.load(BATCH_SIZE, TEST_BATCH_SIZE)

def binarize(images):
    """
    Stochastically binarize values in [0, 1] by treating them as p-values of
    a Bernoulli distribution.
    """
    return (numpy.random.uniform(size=images.shape) < images).astype('float32')

def generate_and_save_samples(tag, seed=None):

    def save_images(images, filename):
        """
        images.shape: (batch, height, width, channels)
        """
        images = images.reshape((10,10,28,28))
        # rowx, rowy, height, width -> rowy, height, rowx, width
        images = images.transpose(1,2,0,3)
        images = images.reshape((10*28, 10*28))

        scipy.misc.toimage(images, cmin=0.0, cmax=1.0).save('{}_{}.jpg'.format(filename, tag))
    samples = numpy.zeros((100, HEIGHT, WIDTH, N_CHANNELS), dtype='float32') if seed is None else numpy.copy(seed[1])
    #samples *= (rng.uniform(output.shape, low=0., high=1.) > 0.3)

    for i in xrange(seed[0] if seed is not None else 0, HEIGHT):
        for j in xrange(WIDTH):
            for k in xrange(N_CHANNELS):
                next_sample = sample_fn(samples)
                samples[:, i, j, k] = next_sample[:, i, j, k]

    save_images(samples, 'samples')

print "Training!" if TRAIN_MODE != 'test' else 'Testing!'
total_iters = 0
total_time = 0.
last_print_time = 0.
last_print_iters = 0
for epoch in itertools.count() if TRAIN_MODE != 'test' else [0]:

    costs = []
    data_feeder = train_data() if TRAIN_MODE != 'test' else test_data()

    for images, targets in data_feeder:
        images = binarize(images.reshape((BATCH_SIZE if TRAIN_MODE != 'test' else TEST_BATCH_SIZE, HEIGHT, WIDTH, N_CHANNELS)))

        start_time = time.time()
        cost = train_fn(images) if TRAIN_MODE != 'test' else eval_fn(images)
        total_time += time.time() - start_time
        total_iters += 1

        costs.append(cost)

        if (TRAIN_MODE=='iters' and total_iters-last_print_iters == PRINT_ITERS) or \
            (TRAIN_MODE=='time' and total_time-last_print_time >= PRINT_TIME):

            dev_costs = []
            if EVAL_DEV_COST:
               for images, targets in dev_data():
                    images = binarize(images.reshape((TEST_BATCH_SIZE, HEIGHT, WIDTH, N_CHANNELS)))
		    dev_cost = eval_fn(images)
		    dev_costs.append(dev_cost)
            else:
                dev_costs.append(0.)

            print "epoch:{}\ttotal iters:{}\ttrain cost:{}\tdev cost:{}\ttotal time:{}\ttime per iter:{}".format(
                epoch,
                total_iters,
                numpy.mean(costs),
                numpy.mean(dev_costs),
                total_time,
                total_time / total_iters
            )

            tag = "iters{}_time{}".format(total_iters, total_time)
            if GEN_SAMPLES:
                generate_and_save_samples(tag)
            if TRAIN_MODE != 'test':
                lib.save_params('{}_params_{}.pkl'.format(PREFIX, tag))

            costs = []
            last_print_time += PRINT_TIME
            last_print_iters += PRINT_ITERS

        if (TRAIN_MODE=='iters' and total_iters == STOP_ITERS) or \
            (TRAIN_MODE=='time' and total_time >= STOP_TIME):

            print "Done!"

            sys.exit()

    if TRAIN_MODE == 'test':
        print "epoch:{}\ttotal iters:{}\ttest cost:{}\ttotal time:{}\ttime per iter:{}".format(
                epoch,
                total_iters,
                numpy.mean(costs),
                total_time,
                total_time / total_iters
            )

        if GEN_SAMPLES:
            tag = "{}".format(PREFIX)
            generate_and_save_samples(tag)

            i = None
            l = 0
            for images, labels in train_data():
                if i is None: i = images
                else: i = numpy.concatenate([i, images], axis=0)
                l += BATCH_SIZE
                if l >= 100:
                    i = binarize(i[:100,:].reshape((100, HEIGHT, WIDTH, N_CHANNELS)))
                    im = numpy.concatenate([i[:,:HEIGHT/2,:,:], numpy.zeros((100, HEIGHT/2, WIDTH, N_CHANNELS)).astype('float32')], axis=1)
                    generate_and_save_samples('masked_' + tag, seed=(HEIGHT/2,im))
                    scipy.misc.toimage(im.reshape((10,10,HEIGHT,WIDTH)).transpose(1,2,0,3).reshape((10*HEIGHT, 10*WIDTH)), cmin=0.0, cmax=1.0).save('{}_{}.jpg'.format('masked', tag))
                    scipy.misc.toimage(i.reshape((10,10,HEIGHT,WIDTH)).transpose(1,2,0,3).reshape((10*HEIGHT, 10*WIDTH)), cmin=0.0, cmax=1.0).save('{}_{}.jpg'.format('groundtruth', tag))
                    scipy.misc.toimage(sample_fn(i).reshape((10, 10, HEIGHT, WIDTH)).transpose(1,2,0,3).reshape((10*HEIGHT,10*WIDTH)), cmin=0.0, cmax=1.0).save('{}_{}.jpg'.format('reconst', tag))
                    break
