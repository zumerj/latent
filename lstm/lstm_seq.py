import theano
from theano import tensor as T
import numpy

from collections import OrderedDict

import imdb

def step_enc(x, hm1, Cm1, W, U, b):
	conc = T.nnet.sigmoid(T.dot(x, W) + T.dot(hm1, U) + b)

	size = conc.shape[0]/4

	Cm1 = conc[:,:size] * conc[:,size:2*size] + conc[:,2*size:3*size] * Cm1
	hm1 = conc[:,3*size:] * T.nnet.sigmoid(Cm1)

	return [hm1, Cm1]

def step_dec(ym1, hm1, Cm1, W, U, b, Wd, bd):
	enc = step_enc(ym1, hm1, Cm1, W, U, b)
	ym1 = T.nnet.sigmoid(T.dot(enc[0], Wd) + bd)
	return [ym1, enc[0], enc[1]]

def init_enc(inp, lat, batch):
	W = theano.shared(name='W', value=numpy.random.uniform(-0.02, 0.02, (inp, lat*4)).astype('float32'))
	U = theano.shared(name='U', value=numpy.random.uniform(-0.02, 0.02, (lat, lat*4)).astype('float32'))
	b = theano.shared(name='b', value=numpy.random.uniform(-0.02, 0.02, (lat*4,)).astype('float32'))

	Cm1 = theano.shared(numpy.zeros((batch, lat)).astype('float32'))
	hm1 = theano.shared(numpy.zeros((batch, lat)).astype('float32'))
	
	return [W, U, b, hm1, Cm1]

def init_dec(inp, lat, batch):
	W = theano.shared(name='Wdec', value=numpy.random.uniform(-0.02, 0.02, (lat, inp)).astype('float32'))
	b = theano.shared(name='bdec', value=numpy.random.uniform(-0.02, 0.02, (inp,)).astype('float32'))
	y = theano.shared(numpy.zeros((inp, batch)).astype('float32'))

	return [y, W, b]

def train(lr):
	x = T.fmatrix('x')
	x_mask = T.fmatrix('x_mask')

	train, valid, test = imdb.load_data(n_words=100000, valid_portion=0.05, maxlen=100, sort_by_len=False)
	data, data_mask, _ = imdb.prepare_data(train[0], train[1], maxlen=100)
	data = numpy.cast['float32'](data)
	data_mask = numpy.cast['float32'](data_mask)
	n_ts = data.shape[0]
	n_samples = data.shape[1]
	lat = 100

	params_enc = init_enc(n_ts, lat, n_samples)
	state, updates = theano.scan(step_enc,
					sequences=[x],
					non_sequences=params_enc[:-2],
					outputs_info=params_enc[-2:])

	hm1 = state[0][-1]
	Cm1 = state[1][-1]

	params_dec = init_dec(n_ts, lat, n_samples)
	y = params_dec[0]
	dstate, dupdates = theano.scan(step_dec,
					outputs_info=[y, hm1, Cm1],
					non_sequences=params_enc[:-2] + params_dec[1:],
					n_steps=n_samples-1)

	cost = T.eq(x[:,1:], T.flatten(dstate[0], 2)).mean(dtype='float32')

	updates_lst = ([(p, p - lr * g) for p, g in zip(params_enc[:-2], theano.grad(cost, wrt=params_enc[:-2]))] + [(params_enc[-2], state[0][-1]), (params_enc[-1], state[1][-1])])
	dupdates_lst = ([(p, p - lr * g) for p, g in zip(params_dec, theano.grad(cost, wrt=params_dec))])

	train_fn = theano.function([x, x_mask], cost, updates=OrderedDict(updates+dupdates), on_unused_input='ignore')
	print "training for 100 epochs..."

	for e in range(100):
		score = train_fn(data, data_mask)
		print "Average correctly predicted: %f" % score

if __name__=='__main__':
	print "training"
	lr = theano.shared(numpy.asarray(1e-3, dtype='float32'))
	train(lr)
	print "DONE"
