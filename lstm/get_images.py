from matplotlib.image import imsave
import numpy
import tables

root = '/rap/jvb-000-aa/data/ImageNet_ILSVRC2010/pylearn2_h5/imagenet_2010_'
opath = 'images'

for i in ["train.h5", "test.h5", "valid.h5"]:
	data = tables.openFile(root + i)
	for ji, j in enumerate(data.root.x):
		imsave(opath + '/batch_' + str(i) + '_img_' + str(ji) + '.png', numpy.asarray(j).swapaxes(0, 1).swapaxes(1, 2))
